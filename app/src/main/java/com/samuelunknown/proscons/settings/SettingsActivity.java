package com.samuelunknown.proscons.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.utilities.PreferenceUtils;
import com.samuelunknown.proscons.utilities.StatusBarUtils;
import com.samuelunknown.proscons.utilities.ToolBarUtils;

import java.lang.reflect.Method;

public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceUtils.setActivityTheme(this, this, true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

//        Log.e(TAG, "AppTheme_Dark_ActionBar: " + R.style.AppTheme_Dark_ActionBar);
//        Log.e(TAG, "AppTheme_Light_ActionBar: " + R.style.AppTheme_Light_ActionBar);
//        Log.e(TAG, "AppTheme_Dark_NoActionBar: " + R.style.AppTheme_Dark_NoActionBar);
//        Log.e(TAG, "AppTheme_Light_NoActionBar: " + R.style.AppTheme_Light_NoActionBar);
//        Log.e(TAG, "AppThemeOverlay_AppBarOverlay: " + R.style.AppThemeOverlay_AppBarOverlay);
//        Log.e(TAG, "AppThemeOverlay_PopupOverlay: " + R.style.AppThemeOverlay_PopupOverlay);
//        Log.e(TAG, "Current theme id: " + getThemeId());

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.settings_activity_toolbar);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region Toolbar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_app_theme_key))) {

            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        }
    }

    /**
     * Определяет id текущей темы
     * см: https://stackoverflow.com/questions/26301345/get-the-theme-value-applied-for-an-activity-programmatically.
     * @return ID текущей темы
     */
    int getThemeId() {
        try {
            Class<?> wrapper = Context.class;
            Method method = wrapper.getMethod("getThemeResId");
            method.setAccessible(true);
            return (Integer) method.invoke(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
