package com.samuelunknown.proscons.adapters;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class DilemmaItemDiffUtilCallback extends DiffUtil.Callback {

    private List<DilemmaListAdapter.DilemmaItem> mOldList;
    private List<DilemmaListAdapter.DilemmaItem> mNewList;

    DilemmaItemDiffUtilCallback(List<DilemmaListAdapter.DilemmaItem> oldList,
                                       List<DilemmaListAdapter.DilemmaItem> newList) {
        this.mOldList = oldList;
        this.mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).getId() == mNewList.get(newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).equals(mNewList.get(newItemPosition));
    }
}
