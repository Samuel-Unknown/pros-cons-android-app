package com.samuelunknown.proscons.adapters;

// информация использовавшаяся для справки:
// http://frogermcs.github.io/recyclerview-animations-androiddevsummit-write-up/
// http://frogermcs.github.io/instamaterial-recyclerview-animations-done-right/

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.samuelunknown.proscons.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DilemmaItemAnimator extends DefaultItemAnimator {

    private final Map<RecyclerView.ViewHolder, AnimatorSet> selectionAnimationsMap = new HashMap<>();
    private final Map<RecyclerView.ViewHolder, AnimatorSet> deselectionAnimationsMap = new HashMap<>();

    @Override
    public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {
        // When we’re animating RecyclerView items we have a chance to ask RecyclerView to keep
        // the previous ViewHolder of the item as-is and provide a new ViewHolder which will
        // animate changes from the previous one (only new ViewHolder will be visible
        // on our RecyclerView). But when we are writing a custom item animator for our
        // layout we should use same ViewHolder and animate the content changes manually.
        // This is why our method returns true in this case.
        return true;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPreLayoutInformation(@NonNull RecyclerView.State state,
                                                     @NonNull RecyclerView.ViewHolder viewHolder,
                                                     int changeFlags, @NonNull List<Object> payloads) {
        if (changeFlags == FLAG_CHANGED) {
            for (Object payload : payloads) {
                if (payload instanceof String) {
                    return new DilemmaItemHolderInfo((String) payload);
                }
            }
        }

        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads);
    }


    @Override
    public boolean animateChange(@NonNull RecyclerView.ViewHolder oldHolder,
                                 @NonNull RecyclerView.ViewHolder newHolder,
                                 @NonNull ItemHolderInfo preInfo,
                                 @NonNull ItemHolderInfo postInfo) {

        if (preInfo instanceof DilemmaItemHolderInfo) {
            cancelCurrentAnimationIfExists(newHolder);
            DilemmaItemHolderInfo dilemmaItemHolderInfo = (DilemmaItemHolderInfo) preInfo;
            DilemmaListAdapter.DilemmaViewHolder holder = (DilemmaListAdapter.DilemmaViewHolder) newHolder;

            if (DilemmaListAdapter.ACTION_SELECT_ITEM_CLICKED.equals(dilemmaItemHolderInfo.updateAction)) {
                animateIconSelection(holder);
            }
            if (DilemmaListAdapter.ACTION_DESELECT_ITEM_CLICKED.equals(dilemmaItemHolderInfo.updateAction)) {
                animateIconDeselection(holder);
            }
        } else {
            return super.animateChange(oldHolder, newHolder, preInfo, postInfo);
        }

        return false;
    }

    private void cancelCurrentAnimationIfExists(RecyclerView.ViewHolder item) {
        if (selectionAnimationsMap.containsKey(item)) {
            selectionAnimationsMap.get(item).cancel();
        }
        if (deselectionAnimationsMap.containsKey(item)) {
            deselectionAnimationsMap.get(item).cancel();
        }
//        ObjectAnimator o = (ObjectAnimator) selectionAnimationsMap.get(holder).getChildAnimations().get(0);
//        o.getCurrentPlayTime();
    }

    private void animateIconSelection(final DilemmaListAdapter.DilemmaViewHolder holder) {
        final AnimatorSet resultAnim = getSelectionAnimatorSet(holder);

        resultAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                selectionAnimationsMap.remove(holder);
                dispatchChangeFinishedIfAllAnimationsEnded(holder);
            }
        });

        resultAnim.start();
        selectionAnimationsMap.put(holder, resultAnim);
    }

    private void animateIconDeselection(final DilemmaListAdapter.DilemmaViewHolder holder) {
        final AnimatorSet resultAnim = getDeselectionAnimatorSet(holder);

        resultAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                deselectionAnimationsMap.remove(holder);
                dispatchChangeFinishedIfAllAnimationsEnded(holder);
            }
        });

        resultAnim.start();
        deselectionAnimationsMap.put(holder, resultAnim);
    }

    private AnimatorSet getSelectionAnimatorSet(@NonNull DilemmaListAdapter.DilemmaViewHolder holder) {
        final Context context = holder.itemView.getContext();
        final Resources r = context.getResources();

        int scaleDuration = r.getInteger(R.integer.icon_appearance_time_full);
        int scaleOffset = r.getInteger(R.integer.icon_appearance_time_offset);
        int rotationDuration = r.getInteger(R.integer.icon_flip_time_full);
        int halfRotationDuration = r.getInteger(R.integer.icon_flip_time_half);

        //region icon Done
        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "alpha", 0, 1);
        alphaAnim.setStartDelay(scaleOffset);
        alphaAnim.setDuration(0);

        ObjectAnimator scaleYAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "scaleY", 0, 1);
        scaleYAnim.setStartDelay(scaleOffset);
        scaleYAnim.setDuration(scaleDuration);
        scaleYAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator scaleXAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "scaleX", 0, 1);
        scaleXAnim.setStartDelay(scaleOffset);
        scaleXAnim.setDuration(scaleDuration);
        scaleXAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        //endregion

        //region icon Front
        ObjectAnimator rotateYFrontAnim = ObjectAnimator.ofFloat(holder.iconFrontTextView, "rotationY", 0, 180);
        rotateYFrontAnim.setDuration(rotationDuration);
        rotateYFrontAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator alphaFrontAnim = ObjectAnimator.ofFloat(holder.iconFrontTextView, "alpha", 1, 0);
        alphaFrontAnim.setStartDelay(halfRotationDuration);
        alphaFrontAnim.setDuration(0);

        ObjectAnimator endRotateYFrontAnim = ObjectAnimator.ofFloat(holder.iconFrontTextView, "rotationY", 180, 0);
        endRotateYFrontAnim.setStartDelay(rotationDuration);
        endRotateYFrontAnim.setDuration(0);
        //endregion

        //region icon Back
        ObjectAnimator rotateYBackAnim = ObjectAnimator.ofFloat(holder.iconBackImageView, "rotationY", -180, 0);
        rotateYBackAnim.setDuration(rotationDuration);
        rotateYBackAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator alphaBackAnim = ObjectAnimator.ofFloat(holder.iconBackImageView, "alpha", 0, 1);
        alphaBackAnim.setStartDelay(halfRotationDuration);
        alphaBackAnim.setDuration(0);
        //endregion

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                rotateYFrontAnim, rotateYBackAnim,
                alphaFrontAnim, alphaBackAnim,
                scaleXAnim, scaleYAnim,
                alphaAnim, endRotateYFrontAnim);

        return animatorSet;
    }

    private AnimatorSet getDeselectionAnimatorSet(@NonNull DilemmaListAdapter.DilemmaViewHolder holder) {
        final Context context = holder.itemView.getContext();
        final Resources r = context.getResources();

        int scaleDuration = r.getInteger(R.integer.icon_disappearance_time_full);
        int scaleOffset = r.getInteger(R.integer.icon_disappearance_time_offset);
        int rotationDuration = r.getInteger(R.integer.icon_flip_time_full);
        int halfRotationDuration = r.getInteger(R.integer.icon_flip_time_half);

        //region icon Done
        ObjectAnimator scaleYAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "scaleY", 1, 0);
        scaleYAnim.setStartDelay(scaleOffset);
        scaleYAnim.setDuration(scaleDuration);
        scaleYAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator scaleXAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "scaleX", 1, 0);
        scaleXAnim.setStartDelay(scaleOffset);
        scaleXAnim.setDuration(scaleDuration);
        scaleXAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(holder.iconDoneImageView, "alpha", 1, 0);
        alphaAnim.setStartDelay(scaleDuration);
        alphaAnim.setDuration(0);
        //endregion

        //region icon Front
        ObjectAnimator rotateYFrontAnim = ObjectAnimator.ofFloat(holder.iconBackImageView, "rotationY", 0, 180);
        rotateYFrontAnim.setDuration(rotationDuration);
        rotateYFrontAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator alphaFrontAnim = ObjectAnimator.ofFloat(holder.iconBackImageView, "alpha", 1, 0);
        alphaFrontAnim.setStartDelay(halfRotationDuration);
        alphaFrontAnim.setDuration(0);

        ObjectAnimator endRotateYFrontAnim = ObjectAnimator.ofFloat(holder.iconBackImageView, "rotationY", 180, 0);
        endRotateYFrontAnim.setStartDelay(rotationDuration);
        endRotateYFrontAnim.setDuration(0);
        //endregion

        //region icon Back
        ObjectAnimator rotateYBackAnim = ObjectAnimator.ofFloat(holder.iconFrontTextView, "rotationY", -180, 0);
        rotateYBackAnim.setDuration(rotationDuration);
        rotateYBackAnim.setInterpolator(new AccelerateDecelerateInterpolator());

        ObjectAnimator alphaBackAnim = ObjectAnimator.ofFloat(holder.iconFrontTextView, "alpha", 0, 1);
        alphaBackAnim.setStartDelay(halfRotationDuration);
        alphaBackAnim.setDuration(0);
        //endregion

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                rotateYFrontAnim, rotateYBackAnim,
                alphaFrontAnim, alphaBackAnim,
                scaleXAnim, scaleYAnim,
                alphaAnim, endRotateYFrontAnim);

        return animatorSet;
    }

    private void dispatchChangeFinishedIfAllAnimationsEnded(DilemmaListAdapter.DilemmaViewHolder holder) {
        if (selectionAnimationsMap.containsKey(holder) || deselectionAnimationsMap.containsKey(holder)) {
            return;
        }

        dispatchAnimationFinished(holder);
    }

    @Override
    public void endAnimation(RecyclerView.ViewHolder item) {
        super.endAnimation(item);
        cancelCurrentAnimationIfExists(item);
    }

    @Override
    public void endAnimations() {
        super.endAnimations();

        // Вот тут может вылететь ConcurrentModificationException
        // хоть и не должен. Это происходит в том случае, если прямое перед закрытием фрагмента
        // с выделенными элементами запустить анимацию снятия выделения.
        // Не стал глубоко копать почему это происходит (TODO подумать)
        // просто перед закрытием фрагмента снимаю выделение без анимации.
//        try {
            for (AnimatorSet animatorSet : selectionAnimationsMap.values()) {
                animatorSet.cancel();
            }

            for (AnimatorSet animatorSet : deselectionAnimationsMap.values()) {
                animatorSet.cancel();
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    public static class DilemmaItemHolderInfo extends ItemHolderInfo {

        /**
         * Тип действия выполненного над Holder'ом.
         * (ACTION_SELECT_ITEM_CLICKED, ACTION_DESELECT_ITEM_CLICKED)
         */
        String updateAction;

        DilemmaItemHolderInfo(String updateAction) {
            this.updateAction = updateAction;
        }
    }
}
