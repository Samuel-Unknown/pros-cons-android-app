package com.samuelunknown.proscons.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.dilemmas.DilemmasPresenter;
import com.samuelunknown.proscons.utilities.AppDateUtils;
import com.samuelunknown.proscons.utilities.ResourcesUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// статья про DiffUtil для использования в отдельном потоке https://medium.com/@jonfhancock/get-threading-right-with-diffutil-423378e126d2
// тут я без него обошёлся, всё таки число дилемм в прилодении вряд ли у кого из пользователей будет громадным

public class DilemmaListAdapter extends RecyclerView.Adapter<DilemmaListAdapter.DilemmaViewHolder> {

    private final DilemmaListAdapterListener mDilemmaListAdapterListener;

    public interface DilemmaListAdapterListener {
        void onDilemmasListClick(int position);
        void onDilemmasListLongClick(int position);
        void onDilemmasListIconClick(int position);
    }

    //region Variables
    private Context mContext;

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = DilemmaListAdapter.class.getSimpleName();

    static final String ACTION_SELECT_ITEM_CLICKED = "action_select_item_clicked";
    static final String ACTION_DESELECT_ITEM_CLICKED = "action_deselect_item_clicked";

    /**
     * Все дилеммы полученные из БД
     */
    private List<DilemmaItem> mAllItems;

    /**
     * Список дилемм видимых пользователю (отличается от mAllItems например тогда,
     * когда включен поиск по дилеммам и введено слово для поиска)
     */
    private List<DilemmaItem> mVisibleItems;

    /**
     * Список элементов которые были удалены, нужен для возможности их восстановления
     */
    private List<DilemmaItem> mCachedItems;

    /**
     * Массив выделенных элементов (ключ - position, значение - true).
     * Например если всего видимых дилемм 10, и выделенны из них 2, то в этом масииве
     * будут лежать два ключа с позициями этих двух элементов и значения для них будут true
     */
    private SparseBooleanArray mSelectedItems;
    //endregion

    //region Comparators
    private static final Comparator<DilemmaItem> ALPHABETICAL_COMPARATOR = new Comparator<DilemmaItem>() {
        @Override
        public int compare(DilemmaItem a, DilemmaItem b) {
            return a.getDescription().compareTo(b.getDescription());
        }
    };

    private static final Comparator<DilemmaItem> TIMESTAMP_COMPARATOR_ASC = new Comparator<DilemmaItem>() {
        @Override
        public int compare(DilemmaItem a, DilemmaItem b) {
            return Long.valueOf(a.getTimeStamp()).compareTo(b.getTimeStamp());
        }
    };

    private static final Comparator<DilemmaItem> TIMESTAMP_COMPARATOR_DESC = new Comparator<DilemmaItem>() {
        @Override
        public int compare(DilemmaItem a, DilemmaItem b) {
            return Long.valueOf(b.getTimeStamp()).compareTo(a.getTimeStamp());
        }
    };
    //endregion

    //region Adapter
    public DilemmaListAdapter(Context context, DilemmaListAdapterListener listener) {
        mContext = context;
        mDilemmaListAdapterListener = listener;
        mAllItems = new ArrayList<>();
        mVisibleItems = new ArrayList<>();
        mCachedItems = new ArrayList<>();
        mSelectedItems = new SparseBooleanArray();
    }

    @Override
    public DilemmaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext); // можно и так -> LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dilemma_list_item, parent, false);

        DilemmaViewHolder dilemmaViewHolder = new DilemmaViewHolder(view);

        setupClickEvents(dilemmaViewHolder);

        return dilemmaViewHolder;
    }

    @Override
    public void onBindViewHolder(DilemmaViewHolder holder, int position) {
        DilemmaItem item = mVisibleItems.get(position);

        final long id             = item.getId();
        final String description  = item.getDescription();
        final float prosPercent   = item.getProsPercent();
        final float consPercent   = item.getConsPercent();
        final int currentState    = item.getCurrentState();
        final long timeStamp      = item.getTimeStamp();


        // тег потом используется для удаления по свайпу (отключил пока)
        holder.itemView.setTag(id); // наверное лучше туда засовывать сразу DilemmaItem

        holder.itemView.setSelected(mSelectedItems.get(position, false));

        holder.descriptionTextView.setText(description/* + " " + getSelectedItemCount()*/);
        holder.timestampTextView.setText(AppDateUtils.getReadableDateString(mContext, timeStamp));
        holder.iconFrontTextView.setText(String.valueOf(description.toUpperCase().charAt(0)));

        holder.iconFrontTextView.setRotationY(0);
        holder.iconBackImageView.setRotationY(0);

        if (prosPercent > consPercent) {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext, R.attr.colorIconPros));
        } else {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext, R.attr.colorIconCons));
        }
    }

    @Override
    public void onBindViewHolder(DilemmaViewHolder holder, int position, List<Object> payloads) {

        if (payloads.isEmpty()) {
            // если текущее обновление холлдера не связанно с выделением/снятием выделения
            // то жёстко устанавливаем какие части иконки должны быть видны
            if (mSelectedItems.get(position)) {
                holder.iconFrontTextView.setAlpha(0);
                holder.iconBackImageView.setAlpha(1f);
                holder.iconDoneImageView.setAlpha(1f);
                holder.iconDoneImageView.setScaleX(1);
                holder.iconDoneImageView.setScaleY(1);
            } else {
                holder.iconFrontTextView.setAlpha(1);
                holder.iconBackImageView.setAlpha(0f);
                holder.iconDoneImageView.setAlpha(0f);
            }
        }

        onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mVisibleItems.size();
    }
    //endregion

    private void setIconTextViewColor(@NonNull DilemmaViewHolder holder, @ColorInt int color) {
        ResourcesUtils.setColorToGradientDrawable(
                (GradientDrawable) holder.iconFrontTextView.getBackground(), color);
    }

    public void swapCursor(@Nullable Cursor cursor) {
        mAllItems.clear();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                final long id             = cursor.getLong(DilemmasPresenter.INDEX_DILEMMA_ID);
                final String description  = cursor.getString(DilemmasPresenter.INDEX_DILEMMA_DESCRIPTION);
                final float prosPercent   = cursor.getFloat(DilemmasPresenter.INDEX_DILEMMA_PROS_PERCENT);
                final float consPercent   = cursor.getFloat(DilemmasPresenter.INDEX_DILEMMA_CONS_PERCENT);
                final int currentState    = cursor.getInt(DilemmasPresenter.INDEX_DILEMMA_CURRENT_STATE);
                final long timeStamp      = cursor.getLong(DilemmasPresenter.INDEX_DILEMMA_TIMESTAMP);

                mAllItems.add(new DilemmaItem(id, description, prosPercent, consPercent, currentState, timeStamp));
            }

            cursor.close();
        }
    }

    // обновляет список видимых дилемм, newList - новый список дилемм которые должны быть отображены
    private void updateVisibleItemsList(@NonNull List<DilemmaItem> newList) {
        List<DilemmaItem> oldItems = new ArrayList<>(mVisibleItems);

        DiffUtil.DiffResult result =
                DiffUtil.calculateDiff(new DilemmaItemDiffUtilCallback(oldItems, newList));

        mVisibleItems.clear();
        mVisibleItems.addAll(newList);
        result.dispatchUpdatesTo(this);
    }

    public void filterByQuery(@Nullable String query) {
        // Если запрос пуст, то все элементы должны быть отображены
        if (query == null) {
            updateVisibleItemsList(mAllItems);
            return;
        }

        // Отбираем те элементы для отображения, которые соответсвуют запросу
        final String lowerCaseQuery = query.toLowerCase();
        final List<DilemmaItem> filteredModelList = new ArrayList<>();

        for (int i = 0; i < mAllItems.size(); i++) {
            DilemmaItem item = mAllItems.get(i);
            final String description  = item.getDescription();
            final String descriptionLowerCase = description.toLowerCase();

            if (descriptionLowerCase.contains(lowerCaseQuery)) {
                filteredModelList.add(item);
            }
        }

        updateVisibleItemsList(filteredModelList);
    }

    private void setupClickEvents(@NonNull final DilemmaViewHolder holder) {
        holder.iconFrontTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int adapterPosition = holder.getAdapterPosition();
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    mDilemmaListAdapterListener.onDilemmasListIconClick(adapterPosition);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int adapterPosition = holder.getAdapterPosition();
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    mDilemmaListAdapterListener.onDilemmasListClick(adapterPosition);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int adapterPosition = holder.getAdapterPosition();
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    mDilemmaListAdapterListener.onDilemmasListLongClick(adapterPosition);
                    return true;
                }
                return false;
            }
        });
    }

    //region Selection
    public int getSelectedItemCount() {
        return mSelectedItems.size();
    }

    public DilemmaItem getDilemmaItem(int position) {
        return mVisibleItems.get(position);
    }

    public List<DilemmaItem> getSelectedItems() {
        List<DilemmaItem> items = new ArrayList<>(mSelectedItems.size());

        for (int i = 0; i < mSelectedItems.size(); i++) {
            items.add(mVisibleItems.get(mSelectedItems.keyAt(i)));
        }

        return items;
    }

    private List<DilemmaItem> getUnselectedItems() {
        List<DilemmaItem> items = new ArrayList<>();

        for (int i = 0; i < mVisibleItems.size(); i++) {
            if (!mSelectedItems.get(i)) {
                items.add(mVisibleItems.get(i));
            }
        }
        return items;
    }

    public List<DilemmaItem> getAllItems() {
        List<DilemmaItem> items = new ArrayList<>(mAllItems.size());
        items.addAll(mAllItems);

        return items;
    }

    public void toggleSelection(int position) {
        if (mSelectedItems.get(position, false)) {
            mSelectedItems.delete(position);
            notifyItemChanged(position, ACTION_DESELECT_ITEM_CLICKED);
        } else {
            mSelectedItems.put(position, true);
            notifyItemChanged(position, ACTION_SELECT_ITEM_CLICKED);
        }
    }

    public void clearSelections() {
        for (int i = 0; i < mVisibleItems.size(); i++) {
            if (mSelectedItems.get(i)) {
                notifyItemChanged(i, ACTION_DESELECT_ITEM_CLICKED);
            }
        }

        mSelectedItems.clear();
    }

    public void clearSelectionsWithoutAnimation() {
        mSelectedItems.clear();
        notifyDataSetChanged();
    }

    private void addItemsToCache(@NonNull List<DilemmaItem> items) {
        mCachedItems.clear();
        mCachedItems.addAll(items);
    }

    private void clearCache() {
        mCachedItems.clear();
    }

    public void removeSelectedItems() {

        List<DilemmaItem> selectedItems = getSelectedItems();
        List<DilemmaItem> unselectedItems = getUnselectedItems();

        if (selectedItems == null || selectedItems.size() == 0) {
            return;
        }

        addItemsToCache(selectedItems);
        mAllItems.removeAll(selectedItems);
        mSelectedItems.clear();

        // Список видимых элементов будет равен unselectedItems
        updateVisibleItemsList(unselectedItems);
    }

    public void removeAllItems() {

        addItemsToCache(mAllItems);
        mAllItems.clear();
        mSelectedItems.clear();

        // Список видимых элементов будет равен unselectedItems
        updateVisibleItemsList(mAllItems);
    }

    public void restoreRemovedItems() {
        mAllItems.addAll(mCachedItems);
        Collections.sort(mAllItems, TIMESTAMP_COMPARATOR_DESC);

        clearCache();

        // Список видимых элементов будет равен mAllItems
        updateVisibleItemsList(mAllItems);
    }

    public List<DilemmaItem> getCachedItems() {
        return mCachedItems;
    }
    //endregion

    class DilemmaViewHolder extends RecyclerView.ViewHolder {

        // Вопросительный текст дилеммы
        TextView descriptionTextView;
        TextView timestampTextView;
        TextView iconFrontTextView;
        ImageView iconBackImageView;
        ImageView iconDoneImageView;

        private DilemmaViewHolder(View itemView) {
            super(itemView);

            descriptionTextView = (TextView) itemView.findViewById(R.id.dilemma_list_item_text_view);
            iconFrontTextView = (TextView) itemView.findViewById(R.id.dilemma_icon_front_text_view);
            timestampTextView = (TextView) itemView.findViewById(R.id.dilemma_list_item_timestamp_text_view);
            iconBackImageView = (ImageView) itemView.findViewById(R.id.dilemma_icon_back_image_view);
            iconDoneImageView = (ImageView) itemView.findViewById(R.id.dilemma_icon_done);
        }
    }

    public class DilemmaItem {

        private final long mId;
        private final String mDescription;
        private final float mProsPercent;
        private final float mConsPercent;
        private final int mCurrentState;
        private final long mTimeStamp;

        public DilemmaItem(final long id, final String description,
                           final float prosPercent, final float consPercent,
                           final int currentState, final long timeStamp) {
            mId = id;
            mDescription = description;
            mProsPercent = prosPercent;
            mConsPercent = consPercent;
            mCurrentState = currentState;
            mTimeStamp = timeStamp;
        }

        public long getId() {
            return mId;
        }

        public String getDescription() {
            return mDescription;
        }

        public float getProsPercent() {
            return mProsPercent;
        }

        public float getConsPercent() {
            return mConsPercent;
        }

        public int getCurrentState() {
            return mCurrentState;
        }

        public long getTimeStamp() {
            return mTimeStamp;
        }

        // методы equals() и hashCode можно сгенерировать автоматически через меню Code->Generate
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DilemmaItem that = (DilemmaItem) o;

            if (mId != that.mId) return false;
            if (Float.compare(that.mProsPercent, mProsPercent) != 0) return false;
            if (Float.compare(that.mConsPercent, mConsPercent) != 0) return false;
            if (mCurrentState != that.mCurrentState) return false;
            if (mTimeStamp != that.mTimeStamp) return false;
            return mDescription != null ? mDescription.equals(that.mDescription) : that.mDescription == null;

        }

        @Override
        public int hashCode() {
            int result = (int) (mId ^ (mId >>> 32));
            result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
            result = 31 * result + (mProsPercent != +0.0f ? Float.floatToIntBits(mProsPercent) : 0);
            result = 31 * result + (mConsPercent != +0.0f ? Float.floatToIntBits(mConsPercent) : 0);
            result = 31 * result + mCurrentState;
            result = 31 * result + (int) (mTimeStamp ^ (mTimeStamp >>> 32));
            return result;
        }
    }
}
