package com.samuelunknown.proscons.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.samuelunknown.proscons.data.DbContract;
import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.dilemma.DilemmaPresenter;
import com.samuelunknown.proscons.utilities.ResourcesUtils;

import java.util.ArrayList;
import java.util.List;

public class ArgumentListAdapter extends RecyclerView.Adapter<ArgumentListAdapter.ArgumentViewHolder> {

    private final ArgumentListAdapterOnClickHandler mClickHandler;

    public interface ArgumentListAdapterOnClickHandler {
        void onArgumentClick(long argumentId);
    }

    private Context mContext;
    // Все дилеммы полученные из БД
    private List<ArgumentItem> mAllItems;

    public void swapCursor(Cursor newCursor) {
        mAllItems.clear();

        if (newCursor != null) {
            while (newCursor.moveToNext()) {
                final long id             = newCursor.getLong(DilemmaPresenter.INDEX_ARGUMENT_ID);
                final String description  = newCursor.getString(DilemmaPresenter.INDEX_ARGUMENT_DESCRIPTION);
                final int importance      = newCursor.getInt(DilemmaPresenter.INDEX_ARGUMENT_IMPORTANCE);
                final String positive     = newCursor.getString(DilemmaPresenter.INDEX_ARGUMENT_POSITIVE);
                final long timeStamp      = newCursor.getLong(DilemmaPresenter.INDEX_ARGUMENT_TIMESTAMP);

                mAllItems.add(new ArgumentItem(id, description, positive, importance, timeStamp));
            }

            newCursor.close();
        }

        notifyDataSetChanged();
    }

    public ArgumentListAdapter(Context context, ArgumentListAdapterOnClickHandler clickHandler) {
        mContext = context;
        mClickHandler = clickHandler;
        mAllItems = new ArrayList<>();
    }

    @Override
    public ArgumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext); // можно и так -> LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.argument_list_item, parent, false);

        return new ArgumentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArgumentViewHolder holder, int position) {
        ArgumentItem item = mAllItems.get(position);

        final long id             = item.getId();
        final String description  = item.getDescription();
        final String positive      = item.getPositive();
        final int importance      = item.getImportance();
        final long timeStamp      = item.getTimeStamp();

        holder.itemView.setTag(id);

        holder.argumentTextView.setText(description);
        holder.weightTextView.setText(String.valueOf(importance));

        if (positive.equals(DbContract.ArgumentEntry.POSITIVE_VALUE)) {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext, R.attr.colorIconPros));
        } else {
            setIconTextViewColor(holder, ResourcesUtils.getColorFromCurrentTheme(mContext, R.attr.colorIconCons));
        }
    }

    @Override
    public int getItemCount() {
        return mAllItems.size();
    }

    private void setIconTextViewColor(@NonNull ArgumentViewHolder holder, int color) {

        ResourcesUtils.setColorToGradientDrawable(
                (GradientDrawable) holder.weightTextView.getBackground(), color);
    }

    public List<ArgumentItem> getAllItems() {
        List<ArgumentItem> items = new ArrayList<>(mAllItems.size());
        items.addAll(mAllItems);

        return items;
    }

    class ArgumentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Текст аргумента
        TextView argumentTextView;
        TextView weightTextView;

        private ArgumentViewHolder(View itemView) {
            super(itemView);

            argumentTextView = (TextView) itemView.findViewById(R.id.argument_list_item_text_view);
            weightTextView = (TextView) itemView.findViewById(R.id.icon_argument_weight_text_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            ArgumentItem argumentItem = getArgumentItem(adapterPosition);

            final long argumentId = argumentItem.getId();

            mClickHandler.onArgumentClick(argumentId);
        }
    }


    public ArgumentItem getArgumentItem(int position) {
        return mAllItems.get(position);
    }

    public class ArgumentItem {

        private final long mId;
        private final String mDescription;
        private final String mPositive;
        private final int mImportance;
        private final long mTimeStamp;

        public ArgumentItem(final long id,
                            final String description,
                            final String positive,
                            final int importance,
                            final long timeStamp) {
            mId = id;
            mDescription = description;
            mPositive = positive;
            mImportance = importance;
            mTimeStamp = timeStamp;
        }

        public long getId() {
            return mId;
        }

        public String getDescription() {
            return mDescription;
        }

        public String getPositive() {
            return mPositive;
        }

        public int getImportance() {
            return mImportance;
        }

        public long getTimeStamp() {
            return mTimeStamp;
        }

        // методы equals() и hashCode можно сгенерировать автоматически через меню Code->Generate

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ArgumentItem that = (ArgumentItem) o;

            if (mId != that.mId) return false;
            if (mImportance != that.mImportance) return false;
            if (mTimeStamp != that.mTimeStamp) return false;
            if (mDescription != null ? !mDescription.equals(that.mDescription) : that.mDescription != null)
                return false;
            return mPositive != null ? mPositive.equals(that.mPositive) : that.mPositive == null;

        }

        @Override
        public int hashCode() {
            int result = (int) (mId ^ (mId >>> 32));
            result = 31 * result + (mDescription != null ? mDescription.hashCode() : 0);
            result = 31 * result + (mPositive != null ? mPositive.hashCode() : 0);
            result = 31 * result + mImportance;
            result = 31 * result + (int) (mTimeStamp ^ (mTimeStamp >>> 32));
            return result;
        }
    }
}
