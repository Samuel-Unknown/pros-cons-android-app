package com.samuelunknown.proscons.dilemmas;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.samuelunknown.proscons.MainActivity;
import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.adapters.DilemmaListAdapter;
import com.samuelunknown.proscons.adapters.DilemmaItemAnimator;

import java.util.ArrayList;
import java.util.List;

public class DilemmasFragment extends Fragment implements
        DilemmasContract.View,
        DilemmasContract.FragmentView,
        DilemmaListAdapter.DilemmaListAdapterListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = DilemmasFragment.class.getSimpleName();

    //region Variables
    private DilemmasPresenter mPresenter;
    private DilemmaListAdapter mDilemmaListAdapter;
    private RecyclerView mDilemmaListRecyclerView;
    private ActionModeCallback mActionModeCallback;
    private ProgressBar mProgressBar;
    private View mFragmentContainer;
    private ActionMode mActionMode;
    private Context mContext;
    private DilemmasContract.FragmentListener mFragmentListener;
    //endregion

    //region Arguments
    private static final String ARG_IS_TRASH_SECTION_REPRESENTATION = "paramIsTrashSectionRepresentation";

    private boolean mParamIsTrashSectionRepresentation;
    //endregion

    //region Create instance of DilemmasFragment
    public DilemmasFragment() {
        // Required empty public constructor
    }

    public static DilemmasFragment newInstance(boolean paramShowDeletedDilemmas) {
        DilemmasFragment fragment = new DilemmasFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_IS_TRASH_SECTION_REPRESENTATION, paramShowDeletedDilemmas);
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    //region Fragment Main Methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DilemmasContract.FragmentListener) {
            mFragmentListener = (DilemmasContract.FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DilemmasContract.FragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragmentListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamIsTrashSectionRepresentation = getArguments().getBoolean(ARG_IS_TRASH_SECTION_REPRESENTATION);
        } else {
            mParamIsTrashSectionRepresentation = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dilemmas, container, false);
        mContext = getActivity();

        mFragmentContainer = getActivity().findViewById(R.id.fragments_container);

        mDilemmaListRecyclerView = view.findViewById(R.id.rv_dilemmas);
        mDilemmaListRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mDilemmaListRecyclerView.setHasFixedSize(true);

        mDilemmaListAdapter = new DilemmaListAdapter(mContext, this);
        mDilemmaListRecyclerView.setAdapter(mDilemmaListAdapter);
        mDilemmaListRecyclerView.setItemAnimator(new DilemmaItemAnimator());

        mProgressBar = view.findViewById(R.id.pb_dilemmas);

        mActionModeCallback = new DilemmasFragment.ActionModeCallback();

        mPresenter = new DilemmasPresenter(this, mContext, getLoaderManager());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mParamIsTrashSectionRepresentation) {
            mPresenter.loadDeletedDilemmas();
        } else {
            mPresenter.loadAvailableDilemmas();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        closeActionMode(false);
    }
    //endregion

    //region Confirmation Dialogs
    private void openDeleteForeverSelectedDilemmasConfirmationDialog() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.delete_dilemmas_confirmation_text)
                .setPositiveButton(R.string.title_action_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteDilemmasForever(mDilemmaListAdapter.getSelectedItems());
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void openDeleteForeverAllDilemmasInTrashConfirmationDialog() {

        if (mDilemmaListAdapter.getAllItems().size() <= 0) {
            return;
        }

        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.delete_all_dilemmas_confirmation_text)
                .setPositiveButton(R.string.title_action_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteDilemmasForever(mDilemmaListAdapter.getAllItems());
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }
    //endregion

    //region Delete Restore Operations
    private void deleteDilemmasForever(List<DilemmaListAdapter.DilemmaItem> itemsToDelete) {
        mPresenter.deleteDilemmasForever(getDilemmasId(itemsToDelete));
    }

    private void markDilemmasAsDeleted(List<DilemmaListAdapter.DilemmaItem> items) {
        mPresenter.markDilemmasAsDeleted(getDilemmasId(items));
    }

    private void markDilemmasAsAvailable(List<DilemmaListAdapter.DilemmaItem> items) {
        mPresenter.markDilemmasAsAvailable(getDilemmasId(items));
    }

    private void undoRestoreOperationMarkDilemmasAsDeleted(List<DilemmaListAdapter.DilemmaItem> items) {
        mPresenter.undoRestoreOperationMarkDilemmasAsDeleted(getDilemmasId(items));
    }

    private void undoDeleteOperationMarkDilemmasAsAvailable(List<DilemmaListAdapter.DilemmaItem> items) {
        mPresenter.undoDeleteOperationMarkDilemmasAsAvailable(getDilemmasId(items));
    }
    //endregion

    //region Action Mode
    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if (mParamIsTrashSectionRepresentation) {
                mode.getMenuInflater().inflate(R.menu.deleted_dilemmas_list_action_mode_menu, menu);
            } else {
                mode.getMenuInflater().inflate(R.menu.available_dilemmas_list_action_mode_menu, menu);
            }
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete_dilemmas:
                    markDilemmasAsDeleted(mDilemmaListAdapter.getSelectedItems());
                    return true;
                case R.id.action_delete_dilemmas_forever:
                    openDeleteForeverSelectedDilemmasConfirmationDialog();
                    return true;
                case R.id.action_restore_dilemmas:
                    markDilemmasAsAvailable(mDilemmaListAdapter.getSelectedItems());
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            mDilemmaListAdapter.clearSelections();
        }
    }

    private void closeActionMode(boolean withDeselectionItemsAnimation) {
        if (mActionMode != null) {
            if (!withDeselectionItemsAnimation) {
                // так как анимация не нужна просто снимаем выделение,
                // и ещё это в некотором роде костыль, ибо если перед закрытием
                // фрагмента вызвать этот метод для того что бы удалить ActionMode,
                // при его удалении начнёт проигрываться анимация, но не успеет проиграться и как
                // итог вылетит эксепшн (ConcurrentModificationException), хоть и не должен
                // вроде как.. глубоко копать не стал, но потом желательно бы разобраться
                // какого хрена он там вылетает. О_о'
                mDilemmaListAdapter.clearSelectionsWithoutAnimation();
            }
            mActionMode.finish();
        }
    }

    private void enableActionMode(int position) {
        if (mActionMode == null) {
            MainActivity activity = (MainActivity)getActivity();
            mActionMode = activity.startSupportActionMode(mActionModeCallback);
        }
        toggleSelection(position);
    }
    //endregion

    //region DilemmasContract.View implementation
    @Override
    public void showLoadingProcess() {
        mProgressBar.setVisibility(View.VISIBLE);
        mDilemmaListRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoadingProcess() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mDilemmaListRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setDilemmas(Cursor cursor) {
        mDilemmaListAdapter.swapCursor(cursor);
    }

    @Override
    public void showMessage(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void clearSearchFilter() {
        mDilemmaListRecyclerView.getLayoutManager().scrollToPosition(0);
        mDilemmaListAdapter.filterByQuery(null);
    }

    @Override
    public void restoreRemovedItems() {
        mDilemmaListAdapter.restoreRemovedItems();
    }

    @Override
    public void showPermanentlyDeletedDilemmasCount(int count) {
        if (count == mDilemmaListAdapter.getSelectedItemCount()) {
            // удалили только выбранные дилеммы
            mDilemmaListAdapter.removeSelectedItems();
        } else {
            // удалили все дилеммы из корзины
            mDilemmaListAdapter.removeAllItems();
        }

        final Snackbar snackbar = Snackbar
                .make(mFragmentContainer,
                        getString(R.string.permanently_delete_selected_dilemmas_text) + ": " + count,
                        Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public void showDeletedDilemmasCount(int count) {
        mDilemmaListAdapter.removeSelectedItems();

        final Snackbar snackbar = Snackbar
                .make(mFragmentContainer,
                        getString(R.string.delete_selected_dilemmas_text) + ": " + count,
                        Snackbar.LENGTH_LONG);

        snackbar.setAction(getString(R.string.action_undo), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // что бы при отмене операции не было глюка с оставшимся
                // визуальным выделением некоторых холдеров (ибо они перееиспользуются)
                // закрываем ActionMode, это в свою очередь вызовет
                // снятие выделения со всех элементов (с анимацией)
                // Сам глюк можно было поймать так: выделям первые два элемента списка,
                // удаляем их, снова выделяем первые два элемента, нажимаем отменить удаление
                // и не смотря на то что выделенных элементов на тот момент всего два,
                // визуально видно буд-то все первые 4 элемента выделены.
                closeActionMode(true);

                undoDeleteOperationMarkDilemmasAsAvailable(mDilemmaListAdapter.getCachedItems());
            }
        });

        snackbar.show();
    }

    @Override
    public void showRestoredDilemmasCount(int count) {
        mDilemmaListAdapter.removeSelectedItems();

        final Snackbar snackbar = Snackbar
                .make(mFragmentContainer,
                        getString(R.string.restored_selected_dilemmas_text) + ": " + count,
                        Snackbar.LENGTH_LONG);

        snackbar.setAction(getString(R.string.action_undo), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // что бы при отмене операции не было глюка с оставшимся
                // визуальным выделением некоторых холдеров (ибо они перееиспользуются)
                // закрываем ActionMode, это в свою очередь вызовет
                // снятие выделения со всех элементов (с анимацией)
                // Сам глюк можно было поймать так: выделям первые два элемента списка,
                // удаляем их, снова выделяем первые два элемента, нажимаем отменить удаление
                // и не смотря на то что выделенных элементов на тот момент всего два,
                // визуально видно буд-то все первые 4 элемента выделены.
                closeActionMode(true);

                undoRestoreOperationMarkDilemmasAsDeleted(mDilemmaListAdapter.getCachedItems());
            }
        });

        snackbar.show();
    }

    @Override
    public void closeActionMode() {
        closeActionMode(true);
    }
    //endregion

    //region DilemmasContract.FragmentView implementation
    @Override
    public void filterByQuery(String query) {
        mDilemmaListAdapter.filterByQuery(query);
        mDilemmaListRecyclerView.getLayoutManager().scrollToPosition(0);
    }

    @Override
    public boolean isTrashSectionRepresents() {
        return mParamIsTrashSectionRepresentation;
    }

    @Override
    public void emptyTrash() {
        openDeleteForeverAllDilemmasInTrashConfirmationDialog();
    }
    //endregion

    //region DilemmaListAdapter.DilemmaListAdapterListener implementation
    @Override
    public void onDilemmasListClick(int position) {
        if (mDilemmaListAdapter.getSelectedItemCount() > 0) {
            toggleSelection(position);
        } else {
            long id = mDilemmaListAdapter.getDilemmaItem(position).getId();
            mFragmentListener.onDilemmaSelected(id);
        }
    }

    @Override
    public void onDilemmasListLongClick(int position) {
        enableActionMode(position);
    }

    @Override
    public void onDilemmasListIconClick(int position) {
        enableActionMode(position);
    }
    //endregion

    private void toggleSelection(int position) {
        mDilemmaListAdapter.toggleSelection(position);
        int count = mDilemmaListAdapter.getSelectedItemCount();

        if (count == 0) {
            mActionMode.finish();
        } else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    private List<String> getDilemmasId(List<DilemmaListAdapter.DilemmaItem> items) {
        List<String> dilemmasId = new ArrayList<>(items.size());
        for (DilemmaListAdapter.DilemmaItem i : items) {
            dilemmasId.add(String.valueOf(i.getId()));
        }

        return dilemmasId;
    }
}