package com.samuelunknown.proscons.dilemmas;

import android.database.Cursor;

import java.util.List;

public interface DilemmasContract {
    interface View {
        void showLoadingProcess();
        void hideLoadingProcess();

        void showMessage(String text);
        void clearSearchFilter();
        void restoreRemovedItems();

        void setDilemmas(Cursor cursor);

        void showPermanentlyDeletedDilemmasCount(int count);
        void showDeletedDilemmasCount(int count);
        void showRestoredDilemmasCount(int count);

        void closeActionMode();
    }

    interface Presenter {
        void loadAvailableDilemmas();
        void loadDeletedDilemmas();

        void deleteDilemmasForever(List<String> dilemmasId);

        void markDilemmasAsDeleted(List<String> dilemmasId);
        void markDilemmasAsAvailable(List<String> dilemmasId);
        void undoRestoreOperationMarkDilemmasAsDeleted(List<String> dilemmasId);
        void undoDeleteOperationMarkDilemmasAsAvailable(List<String> dilemmasId);
    }

    interface FragmentView {
        void filterByQuery(String query);
        boolean isTrashSectionRepresents();
        void emptyTrash();
    }

    interface FragmentListener {
        void onDilemmaSelected(long dilemmaId);
    }
}
