package com.samuelunknown.proscons.dilemmas;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.samuelunknown.proscons.data.DbContract;

import java.lang.ref.WeakReference;
import java.util.List;

public class DilemmasPresenter implements DilemmasContract.Presenter,
        LoaderManager.LoaderCallbacks<Cursor> {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = DilemmasPresenter.class.getSimpleName();

    //region MAIN_DILEMMA_PROJECTION
    private static final String[] MAIN_DILEMMA_PROJECTION = {
            DbContract.DilemmaEntry._ID,
            DbContract.DilemmaEntry.COLUMN_DESCRIPTION,
            DbContract.DilemmaEntry.COLUMN_PROS_PERCENT,
            DbContract.DilemmaEntry.COLUMN_CONS_PERCENT,
            DbContract.DilemmaEntry.COLUMN_CURRENT_STATE,
            DbContract.DilemmaEntry.COLUMN_TIMESTAMP
    };

    // Если порядок строк в массиве MAIN_DILEMMA_PROJECTION изменится,
    // следует изменить и эти индексы
    public static final int INDEX_DILEMMA_ID = 0;
    public static final int INDEX_DILEMMA_DESCRIPTION = 1;
    public static final int INDEX_DILEMMA_PROS_PERCENT = 2;
    public static final int INDEX_DILEMMA_CONS_PERCENT = 3;
    public static final int INDEX_DILEMMA_CURRENT_STATE = 4;
    public static final int INDEX_DILEMMA_TIMESTAMP = 5;
    //endregion

    //region LOADERS ID
    // Уникальные ID для загрузчика данных из БД
    private static final int AVAILABLE_DILEMMAS_LOADER_ID = 10;
    private static final int DELETED_DILEMMAS_LOADER_ID = 11;
    //endregion

    //region TOKENS
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_DELETE_OPERATION = 100;
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_UNDO_DELETE_OPERATION = 200;
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_RESTORE_OPERATION = 300;
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_UNDO_RESTORE_OPERATION = 400;
    private static final int DELETE_DILEMMA_TOKEN = 500;
    //endregion
    //endregion

    //region Variables
    private DilemmasContract.View mView;
    private Context mContext;
    private LoaderManager mLoaderManager;
    private DilemmasAsyncQueryHandler mAsyncQueryHandler;
    //endregion

    DilemmasPresenter(@NonNull DilemmasFragment view, @NonNull Context context,
                             @NonNull LoaderManager loaderManager) {
        mView = view;
        mContext = context;
        mLoaderManager = loaderManager;
        mAsyncQueryHandler = new DilemmasAsyncQueryHandler(this, context.getContentResolver());
    }

    //region DilemmasContract.Presenter implementation
    @Override
    public void loadAvailableDilemmas() {
        startLoader(AVAILABLE_DILEMMAS_LOADER_ID);
    }

    @Override
    public void loadDeletedDilemmas() {
        startLoader(DELETED_DILEMMAS_LOADER_ID);
    }

    @Override
    public void deleteDilemmasForever(List<String> dilemmasId) {
        StringBuilder args = new StringBuilder("?");
        for (int i = 1; i < dilemmasId.size(); i++) {
            args.append(", ?");
        }

        mAsyncQueryHandler.startDelete(
                DELETE_DILEMMA_TOKEN,
                null, DbContract.DilemmaEntry.CONTENT_URI,
                DbContract.DilemmaEntry._ID + " IN(" + args + ")",
                dilemmasId.toArray(new String[dilemmasId.size()]));
    }

    @Override
    public void markDilemmasAsDeleted(List<String> dilemmasId) {
        updateDilemmasCurrentState(
                UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_DELETE_OPERATION,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED,
                dilemmasId);
    }

    @Override
    public void markDilemmasAsAvailable(List<String> dilemmasId) {
        updateDilemmasCurrentState(
                UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_RESTORE_OPERATION,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE,
                dilemmasId);
    }

    @Override
    public void undoRestoreOperationMarkDilemmasAsDeleted(List<String> dilemmasId) {
        updateDilemmasCurrentState(
                UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_UNDO_RESTORE_OPERATION,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED,
                dilemmasId);
    }

    @Override
    public void undoDeleteOperationMarkDilemmasAsAvailable(List<String> dilemmasId) {
        updateDilemmasCurrentState(
                UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_UNDO_DELETE_OPERATION,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE,
                dilemmasId);
    }
    //endregion

    private void startLoader(int loaderId) {
        Loader loader = mLoaderManager.getLoader(loaderId);
        if (loader != null) {
            mLoaderManager.restartLoader(loaderId, null, this);
        } else {
            mLoaderManager.initLoader(loaderId, null, this);
        }
    }

    /**
     * Обновляет состояние дилемм
     * @param updateToken токен используемы для асинхронной опреации Update'а
     *                    (UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED или UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE)
     * @param newState новое состояние для дилемм(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_***)
     * @param dilemmasId id дилеем состояние которых нужно обновить
     */
    private void updateDilemmasCurrentState(int updateToken, String newState,
                                            List<String> dilemmasId) {

        if (!newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_ARCHIVED) &&
                !newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE) &&
                !newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED)) {
            throw new UnsupportedOperationException("updateDilemmasCurrentState(): unsupported newState -> " + newState);
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(
                DbContract.DilemmaEntry.COLUMN_CURRENT_STATE,
                newState);

        StringBuilder args = new StringBuilder("?");
        for (int i = 1; i < dilemmasId.size(); i++) {
            args.append(", ?");
        }

        mAsyncQueryHandler.startUpdate(
                updateToken,
                null,
                DbContract.DilemmaEntry.CONTENT_URI,
                contentValues,
                DbContract.DilemmaEntry._ID + " IN(" + args + ")",
                dilemmasId.toArray(new String[dilemmasId.size()]));

        // Правильный способ конвертирования List<String> в String[]
        // dilemmasId.toArray(new String[dilemmasId.size()]);
    }

    //region Cursor Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        mView.showLoadingProcess();

        switch (id) {
            case AVAILABLE_DILEMMAS_LOADER_ID:
                return new CursorLoader(mContext,
                        DbContract.DilemmaEntry.CONTENT_URI,
                        MAIN_DILEMMA_PROJECTION,
                        DbContract.DilemmaEntry.COLUMN_CURRENT_STATE + "=?",
                        new String[]{DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE},
                        DbContract.DilemmaEntry.COLUMN_TIMESTAMP + " DESC");

            case DELETED_DILEMMAS_LOADER_ID:
                return new CursorLoader(mContext,
                        DbContract.DilemmaEntry.CONTENT_URI,
                        MAIN_DILEMMA_PROJECTION,
                        DbContract.DilemmaEntry.COLUMN_CURRENT_STATE + "=?",
                        new String[]{DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED},
                        DbContract.DilemmaEntry.COLUMN_TIMESTAMP + " DESC");

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mView.setDilemmas(data);
        mView.hideLoadingProcess();
        mView.clearSearchFilter();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mView.setDilemmas(null);
    }
    //endregion

    //region Update & Delete Completed
    private void onUpdateCompleted(int token, int updatedRows) {
        switch (token) {
            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_DELETE_OPERATION
            case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_DELETE_OPERATION: {
                if (updatedRows > 0) {
                    mView.showDeletedDilemmasCount(updatedRows);
                } else {
                    mView.showMessage("Error: dilemmas didn't mark as deleted");
                }

                mView.closeActionMode();
                break;
            }
            //endregion

            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_UNDO_DELETE_OPERATION
            case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_UNDO_DELETE_OPERATION: {
                if (updatedRows > 0) {
                    mView.restoreRemovedItems();
                } else {
                    mView.showMessage("Error: dilemmas didn't mark as available");
                }
                break;
            }
            //endregion

            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_RESTORE_OPERATION
            case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE_RESTORE_OPERATION: {
                if (updatedRows > 0) {
                    mView.showRestoredDilemmasCount(updatedRows);
                } else {
                    mView.showMessage("Error: dilemmas didn't mark as available");
                }

                mView.closeActionMode();
                break;
            }
            //endregion

            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_UNDO_RESTORE_OPERATION
            case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED_UNDO_RESTORE_OPERATION: {
                if (updatedRows > 0) {
                    mView.restoreRemovedItems();
                } else {
                    mView.showMessage("Error: dilemmas didn't mark as deleted");
                }
                break;
            }
            //endregion
        }
    }

    private void onDeleteCompleted(int token, int deletedRows) {
        switch (token) {
            case DELETE_DILEMMA_TOKEN: {
                if (deletedRows > 0) {
                    mView.showPermanentlyDeletedDilemmasCount(deletedRows);
                } else {
                    mView.showMessage("Error: dilemmas didn't delete");
                }

                mView.closeActionMode();
                break;
            }
        }
    }
    //endregion

    private static class DilemmasAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<DilemmasPresenter> mPresenterRef;

        DilemmasAsyncQueryHandler(DilemmasPresenter presenter, ContentResolver cr) {
            super(cr);

            // берём weak reference что бы потом по завершении операций вставки,
            // обновления или удаления можно было вызвать методы класса DilemmasPresenter,
            // если его экземпляр ещё не был удалён
            mPresenterRef = new WeakReference<>(presenter);
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);

            DilemmasPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onUpdateCompleted(token, result);
            }
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            super.onDeleteComplete(token, cookie, result);

            DilemmasPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onDeleteCompleted(token, result);
            }
        }
    }
}
