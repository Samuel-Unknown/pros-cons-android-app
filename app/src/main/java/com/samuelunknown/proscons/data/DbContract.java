package com.samuelunknown.proscons.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Класс определяющий таблицы и имена столбцов для базы данных дилемм.
 */
public class DbContract {

    public static final String CONTENT_AUTHORITY = "com.samuelunknown.proscons";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_DILEMMA                     = "dilemma";
    public static final String PATH_ARGUMENT                    = "argument";
    public static final String PATH_PROS_CONS_TOTAL_VALUES      = "pros_cons_total_values";

    /**
     * Таблица Дилемм
     */
    public static final class DilemmaEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_DILEMMA)
                .build();

        /*********************** Таблица ***********************/
        public static final String TABLE_NAME = "dilemma";
        /*********************** Колонки ***********************/
        // TEXT
        public static final String COLUMN_DESCRIPTION   = "description";
        // INTEGER min = 0, max = 100, default = 50. Хранит процент положительных аргументов,
        public static final String COLUMN_PROS_PERCENT  = "pros_percent";
        // INTEGER min = 0, max = 100, default = 50. Хранит процент отрицательных аргументов,
        public static final String COLUMN_CONS_PERCENT  = "cons_percent";
        // INTEGER enum (0 - Available, 1 - Archived, 2 - Deleted), default = 0
        public static final String COLUMN_CURRENT_STATE = "current_state";
        // INTEGER default = CURRENT_TIMESTAMP (но почему-то записывается только год.. )
        public static final String COLUMN_TIMESTAMP     = "timestamp";
        /********************************************************/

        public static final String VALUE_CURRENT_STATE_AVAILABLE = "0";
        public static final String VALUE_CURRENT_STATE_ARCHIVED  = "1";
        public static final String VALUE_CURRENT_STATE_DELETED   = "2";
    }

    /**
     * Таблица Аргументов
     */
    public static final class ArgumentEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_ARGUMENT)
                .build();

        /*********************** Таблица ***********************/
        public static final String TABLE_NAME   = "argument";
        /*********************** Колонки ***********************/
        // INTEGER FOREIGN KEY
        public static final String COLUMN_ID_DILEMMA    = "id_dilemma";
        // TEXT
        public static final String COLUMN_DESCRIPTION   = "description";
        // INTEGER enum (0 - Negative, 1 - Positive), default = 1
        public static final String COLUMN_POSITIVE      = "positive";
        // INTEGER min = 1, max = 10, default = 1
        public static final String COLUMN_IMPORTANCE    = "importance";
        // INTEGER default = CURRENT_TIMESTAMP (но почему-то записывается только год.. )
        public static final String COLUMN_TIMESTAMP     = "timestamp";
        /********************************************************/

        // значения для колонки COLUMN_POSITIVE
        public static final String POSITIVE_VALUE       = "1";
        public static final String NEGATIVE_VALUE       = "0";
    }

    // Названия колонок для выборки суммарных значений весов аргументов конкретной дилеммы
    public static final String COLUMN_TOTAL_POSITIVE = "total_positive";
    public static final String COLUMN_TOTAL_NEGATIVE = "total_negative";

    public static final Uri PROS_CONS_TOTAL_VALUES_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
            .appendPath(PATH_PROS_CONS_TOTAL_VALUES)
            .build();
}
