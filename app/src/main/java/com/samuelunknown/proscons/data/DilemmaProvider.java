package com.samuelunknown.proscons.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DilemmaProvider extends ContentProvider {

    //region Const Values
    // Эти константы будут использованы для сопоставления URI и данных на которые URI указывает
    public static final int CODE_DILEMMA = 100;
    public static final int CODE_DILEMMA_WITH_ID = 101;

    public static final int CODE_ARGUMENT = 200;
    public static final int CODE_ARGUMENT_WITH_ID = 201;

    // Этот код сопоставления служит для получения значений суммы всех положительных и
    // всех отрицательных весов по аргументам привязанным к опеределённой делемме
    public static final int CODE_PROS_CONS_TOTAL_VALUES_WITH_DILEMMA_ID = 301;
    //endregion

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private DilemmaDbHelper mDilemmaDbHelper;

    public static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DbContract.CONTENT_AUTHORITY;

        // Этот URI -> content://com.samuelunknown.proscons/dilemma
        matcher.addURI(authority, DbContract.PATH_DILEMMA, CODE_DILEMMA);

        // Этот URI -> content://com.samuelunknown.proscons/dilemma/131 (131 выбранно случайно, тут может быть любое число)
        matcher.addURI(authority, DbContract.PATH_DILEMMA + "/#", CODE_DILEMMA_WITH_ID);

        // Этот URI -> content://com.samuelunknown.proscons/argument
        matcher.addURI(authority, DbContract.PATH_ARGUMENT, CODE_ARGUMENT);

        // Этот URI -> content://com.samuelunknown.proscons/argument/131 (131 выбранно случайно, тут может быть любое число)
        matcher.addURI(authority, DbContract.PATH_ARGUMENT + "/#", CODE_ARGUMENT_WITH_ID);

        // Этот URI -> content://com.samuelunknown.proscons/pros_cons_total_values/131 (131 выбранно случайно, тут может быть любое число,
        // при этом под 131 тут понимает ID дилеммы по которой высчитываютя суммы)
        matcher.addURI(authority, DbContract.PATH_PROS_CONS_TOTAL_VALUES + "/#", CODE_PROS_CONS_TOTAL_VALUES_WITH_DILEMMA_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDilemmaDbHelper = new DilemmaDbHelper(getContext());
        return true;
    }

    // Параметры selection и selectionArgs игнорируются
    // для Uri ссылающихся на конкретную дилемму или аргумент
    // Примеры Uri с сылкой на коретную дилему и аргумент:
    // content://com.samuelunknown.proscons/dilemma/131,
    // content://com.samuelunknown.proscons/argument/131)
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase db = mDilemmaDbHelper.getReadableDatabase();

        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            case CODE_DILEMMA: {

                retCursor = db.query(DbContract.DilemmaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case CODE_DILEMMA_WITH_ID: {
                String id_dilemma = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_dilemma);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong dilemma id: " + id_dilemma);
                }
                //endregion

                retCursor = db.query(DbContract.DilemmaEntry.TABLE_NAME,
                        projection,
                        DbContract.DilemmaEntry._ID + "=?",
                        new String[]{id_dilemma},
                        null,
                        null,
                        null);
                break;
            }
            case CODE_ARGUMENT: {
                retCursor =  db.query(DbContract.ArgumentEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            }
            case CODE_ARGUMENT_WITH_ID: {
                String id_argument = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_argument);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong argument id: " + id_argument);
                }
                //endregion

                retCursor = db.query(DbContract.ArgumentEntry.TABLE_NAME,
                        projection,
                        DbContract.ArgumentEntry._ID + "=?",
                        new String[]{id_argument},
                        null,
                        null,
                        null);
                break;
            }
            case CODE_PROS_CONS_TOTAL_VALUES_WITH_DILEMMA_ID: {
                String id_dilemma = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_dilemma);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong dilemma id: " + id_dilemma);
                }
                //endregion

                if (projection == null || projection.length < 2) {
                    projection = new String[] {
                            DbContract.COLUMN_TOTAL_POSITIVE,
                            DbContract.COLUMN_TOTAL_NEGATIVE };
                }

                String query =
                        "SELECT " +
                        "(SELECT SUM(" + DbContract.ArgumentEntry.COLUMN_IMPORTANCE + ") " +
                                "FROM " + DbContract.ArgumentEntry.TABLE_NAME + " " +
                                "WHERE " + DbContract.ArgumentEntry.COLUMN_ID_DILEMMA +
                                    " =? " +
                                "AND " + DbContract.ArgumentEntry.COLUMN_POSITIVE +
                                    " = " + DbContract.ArgumentEntry.POSITIVE_VALUE + ") as " + projection[0] + ", " +
                        "(SELECT SUM(" + DbContract.ArgumentEntry.COLUMN_IMPORTANCE + ") " +
                                "FROM " + DbContract.ArgumentEntry.TABLE_NAME + " " +
                                "WHERE " + DbContract.ArgumentEntry.COLUMN_ID_DILEMMA +
                                    " =? " +
                                "AND " + DbContract.ArgumentEntry.COLUMN_POSITIVE +
                                    " = " + DbContract.ArgumentEntry.NEGATIVE_VALUE + ") as " + projection[1] + " ";

//                В итоге запрос будет выглядеть так:
//                SELECT
//                    (SELECT SUM(`importance`) FROM `argument` WHERE `id_dilemma` =? AND `positive` = 1) as `total_positive`,
//                    (SELECT SUM(`importance`) FROM `argument` WHERE `id_dilemma` =? AND `positive` = 0) as `total_negative`;

                retCursor = db.rawQuery(query, new String[]{id_dilemma, id_dilemma});
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in query()");
        }

        retCursor.setNotificationUri(context.getContentResolver(), uri);

        return retCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        throw new RuntimeException(
                "We are not implementing getType.");
    }

    // Uri должны ссылаться либо на таблицу с дилеммами, либо на таблицу с аргументами
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {

        final SQLiteDatabase db = mDilemmaDbHelper.getWritableDatabase();

        Uri returnUri;

        switch (sUriMatcher.match(uri)) {
            case CODE_DILEMMA: {
                long id = db.insert(DbContract.DilemmaEntry.TABLE_NAME, null, values);
                if ( id > 0 ) {
                    returnUri = ContentUris.withAppendedId(DbContract.DilemmaEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }

            case CODE_ARGUMENT: {
                long id = db.insert(DbContract.ArgumentEntry.TABLE_NAME, null, values);
                if ( id > 0 ) {
                    returnUri = ContentUris.withAppendedId(DbContract.ArgumentEntry.CONTENT_URI, id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in insert()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return returnUri;
    }

    // Параметры selection и selectionArgs игнорируются
    // Примеры Uri с сылкой на коретную дилему и аргумент:
    // content://com.samuelunknown.proscons/dilemma/131,
    // content://com.samuelunknown.proscons/argument/131)
    // При ссылке без конкретики данные из таблиц удаляются целиком
    // При удалении сразу всех дилемм, удаляются и все аргументы
    // При удалениии конкретной дилеммы, удаляются аргументы которые на неё ссылаются
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int numRowsDeleted;

        switch (sUriMatcher.match(uri)) {

            case CODE_DILEMMA: {
                final SQLiteDatabase db = mDilemmaDbHelper.getWritableDatabase();

                String selectionForArguments;

                if (selection != null && selectionArgs != null) {
                    String args = "?";
                    for (int i = 1; i < selectionArgs.length; i++) {
                        args += ", ?";
                    }
                    selectionForArguments = DbContract.ArgumentEntry.COLUMN_ID_DILEMMA + " IN(" + args + ")";

                } else {
                    /*
                     * Если передать null в качестве selection в SQLiteDatabase#available_dilemmas_list_action_mode_menu, все данные из таблицы
                     * будет удалены, но мы не узнаем сколько точно строк было удалено. Согласно документации к
                     * SQLiteDatabase, если передать в качестве selection 1, то будут удалены все строки, но при
                     * этом мы точно узнаем сколько строк удалилось.
                     */
                    selection = "1";
                    selectionArgs = new String[]{};

                    selectionForArguments = "1";
                }

                db.beginTransaction();
                try {
                    // Удаляем все аргументы
                    db.delete(
                            DbContract.ArgumentEntry.TABLE_NAME,
                            selectionForArguments,
                            selectionArgs);

                    // Удаляем все дилеммы
                    numRowsDeleted = db.delete(
                            DbContract.DilemmaEntry.TABLE_NAME,
                            selection,
                            selectionArgs);

                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                break;
            }

            case CODE_DILEMMA_WITH_ID: {
                final SQLiteDatabase db = mDilemmaDbHelper.getWritableDatabase();
                String id_dilemma = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_dilemma);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong dilemma id: " + id_dilemma);
                }
                //endregion

                db.beginTransaction();
                try {
                    // удаляем аргументы ссылающиеся на эту дилему
                    db.delete(
                            DbContract.ArgumentEntry.TABLE_NAME,
                            DbContract.ArgumentEntry.COLUMN_ID_DILEMMA + "=?",
                            new String[]{id_dilemma});

                    // удаляем саму дилему с заданным _ID
                    numRowsDeleted = db.delete(
                            DbContract.DilemmaEntry.TABLE_NAME,
                            DbContract.DilemmaEntry._ID + "=?",
                            new String[]{id_dilemma});

                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }

                break;
            }

            case CODE_ARGUMENT: {
                // удаляем все аргументы из БД
                numRowsDeleted = mDilemmaDbHelper.getWritableDatabase().delete(
                        DbContract.ArgumentEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                break;
            }

            case CODE_ARGUMENT_WITH_ID: {
                String id_argument = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_argument);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong argument id: " + id_argument);
                }
                //endregion

                // удаляем аргумент с коретным _ID
                numRowsDeleted = mDilemmaDbHelper.getWritableDatabase().delete(
                        DbContract.ArgumentEntry.TABLE_NAME,
                        DbContract.ArgumentEntry._ID + "=?",
                        new String[]{id_argument});
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }


        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in delete()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return numRowsDeleted;
    }

    // Примеры Uri с сылкой на коретную дилему и аргумент:
    // content://com.samuelunknown.proscons/dilemma/131,
    // content://com.samuelunknown.proscons/argument/131)
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        int numRowsUpdated;

        switch (sUriMatcher.match(uri)) {
            case CODE_DILEMMA: {
                // этот вариант используется когда разом помечаются дилеммы например как удалённые
                numRowsUpdated = mDilemmaDbHelper.getWritableDatabase().update(
                        DbContract.DilemmaEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            }
            case CODE_DILEMMA_WITH_ID:
                String id_dilemma = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_dilemma);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong dilemma id: " + id_dilemma);
                }
                //endregion

                numRowsUpdated = mDilemmaDbHelper.getWritableDatabase().update(
                        DbContract.DilemmaEntry.TABLE_NAME,
                        values,
                        DbContract.DilemmaEntry._ID + "=?",
                        new String[]{id_dilemma});
                break;
            case CODE_ARGUMENT_WITH_ID:
                String id_argument = uri.getPathSegments().get(1);

                //region Check ID is a value
                try {
                    long id = Long.valueOf(id_argument);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new UnsupportedOperationException("Unknown uri: " + uri + ", Wrong argument id: " + id_argument);
                }
                //endregion

                numRowsUpdated = mDilemmaDbHelper.getWritableDatabase().update(
                        DbContract.ArgumentEntry.TABLE_NAME,
                        values,
                        DbContract.ArgumentEntry._ID + "=?",
                        new String[]{id_argument});
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context context = getContext();
        if (context == null) {
            throw new NullPointerException(this.getClass().getSimpleName() + ": Context is null in update()");
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return numRowsUpdated;
    }
}
