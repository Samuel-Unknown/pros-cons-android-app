package com.samuelunknown.proscons.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.utilities.AppDateUtils;

import java.util.Locale;

import static com.samuelunknown.proscons.data.DbContract.*;

/**
 * Класс для управления локальной БД с таблицами дилемм и аргументов
 */
public class DilemmaDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "dilemma.db";

    /**
     * Контекст приложения
     */
    private Context mContext;

    /**
     * Версия БД приложения.
     * Каждый раз внося изменения в структуру БД, следует увеличивать DATABASE_VERSION на 1.
     * Первая версия имела номер = 1.
     * Кроме того следует добавить метод апгрейда до новой версии БД в onUpgrade().
     */
    private static final int DATABASE_VERSION = 2;

    public DilemmaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_DILEMMA_TABLE =
                "CREATE TABLE " + DilemmaEntry.TABLE_NAME + " (" +

                DilemmaEntry._ID                  + " INTEGER PRIMARY KEY AUTOINCREMENT, "         +
                DilemmaEntry.COLUMN_DESCRIPTION   + " TEXT NOT NULL, "                             +
                DilemmaEntry.COLUMN_PROS_PERCENT  + " INTEGER NOT NULL DEFAULT 50,"                +
                DilemmaEntry.COLUMN_CONS_PERCENT  + " INTEGER NOT NULL DEFAULT 50,"                +
                DilemmaEntry.COLUMN_CURRENT_STATE + " INTEGER NOT NULL DEFAULT 0,"                 +
                DilemmaEntry.COLUMN_TIMESTAMP     + " INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP " +
                ");";

        final String SQL_CREATE_ARGUMENT_TABLE =
                "CREATE TABLE " + ArgumentEntry.TABLE_NAME + " (" +

                ArgumentEntry._ID                + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ArgumentEntry.COLUMN_ID_DILEMMA  + " INTEGER NOT NULL, "                  +
                ArgumentEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, "                     +
                ArgumentEntry.COLUMN_POSITIVE    + " INTEGER NOT NULL DEFAULT 1,"         +
                ArgumentEntry.COLUMN_IMPORTANCE  + " INTEGER NOT NULL DEFAULT 1, "        +
                ArgumentEntry.COLUMN_TIMESTAMP   + " INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP, " +

                "FOREIGN KEY(" + ArgumentEntry.COLUMN_ID_DILEMMA + ") " +
                "REFERENCES "  + DilemmaEntry.TABLE_NAME         + "("  + DilemmaEntry._ID + ")" +
                ");";

        db.execSQL(SQL_CREATE_DILEMMA_TABLE);
        db.execSQL(SQL_CREATE_ARGUMENT_TABLE);

        insertExampleDataAboutBike(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Последовательный апгрейд БД
        // 1 -> 2 -> 3-> ... -> n
        switch (oldVersion) {
            case 1:
                onUpgradeTo2(db);
        }
    }

    /**
     * Записывает в БД демонстрационные данные (одну дилемму о покупке веллосипеда)
     * @param db база данных
     */
    private void insertExampleDataAboutBike(SQLiteDatabase db) {

        //region Вставка дилеммы
        long dilemmaId = insertExampleDilemma(
                db,
                mContext.getString(R.string.example_db_data_dilemma_description),
                65.2f,
                34.8f);
        //endregion

        //region Вставка аргументов
        insertExampleArgument(db, dilemmaId,
                mContext.getString(R.string.example_db_data_argument_1_description),
                Integer.valueOf(ArgumentEntry.POSITIVE_VALUE),
                7);

        insertExampleArgument(db, dilemmaId,
                mContext.getString(R.string.example_db_data_argument_2_description),
                Integer.valueOf(ArgumentEntry.POSITIVE_VALUE),
                4);

        insertExampleArgument(db, dilemmaId,
                mContext.getString(R.string.example_db_data_argument_3_description),
                Integer.valueOf(ArgumentEntry.POSITIVE_VALUE),
                4);

        insertExampleArgument(db, dilemmaId,
                mContext.getString(R.string.example_db_data_argument_4_description),
                Integer.valueOf(ArgumentEntry.NEGATIVE_VALUE),
                5);

        insertExampleArgument(db, dilemmaId,
                mContext.getString(R.string.example_db_data_argument_5_description),
                Integer.valueOf(ArgumentEntry.NEGATIVE_VALUE),
                3);
        //endregion
    }

    /**
     * Вставляет дилемму в качестве демонстрационных данных
     * @param db база данных
     * @param dilemmaDescription описание дилеммы
     * @param prosPercent суммарный процент по всем положительным аргументам
     * @param consPercent суммарный процент по всем отрицательным аргументам
     * @return id вставленной дилеммы
     */
    private long insertExampleDilemma(SQLiteDatabase db, String dilemmaDescription,
                                float prosPercent, float consPercent) {
        ContentValues dilemmaContentValues = new ContentValues();

        dilemmaContentValues.put(DilemmaEntry.COLUMN_DESCRIPTION,
                dilemmaDescription);
        dilemmaContentValues.put(DilemmaEntry.COLUMN_PROS_PERCENT,
                String.format(Locale.ENGLISH, "%.1f", prosPercent));
        dilemmaContentValues.put(DilemmaEntry.COLUMN_CONS_PERCENT,
                String.format(Locale.ENGLISH, "%.1f", consPercent));
        dilemmaContentValues.put(DilemmaEntry.COLUMN_TIMESTAMP,
                AppDateUtils.getGmtForCurrentTime());

        long dilemmaId = db.insert(DilemmaEntry.TABLE_NAME, null, dilemmaContentValues);

        return dilemmaId;
    }

    /**
     * Вставляет аргумент в качестве демонстрационных данных
     * @param db база данных
     * @param dilemmaId ID дилеммы в БД
     * @param argumentDescription описание аргумента
     * @param positive положительный это аргумент или нет, 0 - отрицательный, 1 - положительный
     * @param importance важность (от 1 до 10)
     */
    private void insertExampleArgument(SQLiteDatabase db, long dilemmaId, String argumentDescription,
                                       int positive, int importance) {
        ContentValues argumentContentValues = new ContentValues();

        argumentContentValues.put(DbContract.ArgumentEntry.COLUMN_ID_DILEMMA,
                dilemmaId);
        argumentContentValues.put(DbContract.ArgumentEntry.COLUMN_DESCRIPTION,
                argumentDescription);
        argumentContentValues.put(DbContract.ArgumentEntry.COLUMN_POSITIVE,
                positive);
        argumentContentValues.put(DbContract.ArgumentEntry.COLUMN_IMPORTANCE,
                importance);
        argumentContentValues.put(DbContract.ArgumentEntry.COLUMN_TIMESTAMP,
                AppDateUtils.getGmtForCurrentTime());

        db.insert(DbContract.ArgumentEntry.TABLE_NAME, null, argumentContentValues);
    }

    /**
     * Обновление версии БД с версии 1, до версии 2
     * @param db база данных
     */
    private void onUpgradeTo2(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DilemmaEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ArgumentEntry.TABLE_NAME);
        onCreate(db);
    }

}
