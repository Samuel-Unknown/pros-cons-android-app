package com.samuelunknown.proscons.dilemma;

import android.database.Cursor;

public interface DilemmaContract {
    interface View {
        void onDilemmaMarkedAsDeleted(String dilemmaId);
        void onDilemmaMarkedAsAvailable(String dilemmaId);

        void setProsAndConsPercent(float prosPercent, float consPercent);
        void setDilemmaDescription(String description);
        void setProsArguments(Cursor cursor);
        void setConsArguments(Cursor cursor);

        void showMessage(String text);
        void showLoadingProcess();
        void hideLoadingProcess();
    }

    interface Presenter {
        void markDilemmaAsAvailable();
        void markDilemmaAsDeleted();
        void loadData(String dilemmaId);
    }
}