package com.samuelunknown.proscons.dilemma;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.samuelunknown.proscons.dilemmas.DilemmasFragment;
import com.samuelunknown.proscons.MainActivity;
import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.adapters.ArgumentListAdapter;
import com.samuelunknown.proscons.result_view.ResultView;

import java.util.List;

public class DilemmaFragment
        extends
            Fragment
        implements
            DilemmaContract.View,
            ArgumentListAdapter.ArgumentListAdapterOnClickHandler {

    public interface OnDilemmaFragmentListener {
        void onClickAddArgument(String dilemmaId);
        void onClickEditArgument(String dilemmaId, String argumentId);
        void onClickEditDilemmaDescription(String dilemmaId, String currentDescription);
        void onDilemmaMarkedAsAvailable(String dilemmaId);
        void onDilemmaMarkedAsDeleted(String dilemmaId);
    }

    //region Variables
    private Context mContext;

    private TextView mDilemmaTextView;
    private ResultView mResultView;
    private ArgumentListAdapter mArgumentListAdapterPros;

    private RecyclerView mArgumentListRecyclerViewPros;
    private ArgumentListAdapter mArgumentListAdapterCons;

    private RecyclerView mArgumentListRecyclerViewCons;
    private ProgressBar mProgressBar;

    private Button mButtonAddArgument;
    private DilemmaContract.Presenter mPresenter;

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String ARG_DILEMMA_ID = "paramDilemmaID";
    private String mParamDilemmaId;

    private OnDilemmaFragmentListener mListener;
    //endregion

    //region Create instance of DilemmaFragment
    public DilemmaFragment() {
        // Required empty public constructor
    }

    public static DilemmaFragment newInstance(String paramDilemmaId) {
        DilemmaFragment fragment = new DilemmaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DILEMMA_ID, paramDilemmaId);
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDilemmaFragmentListener) {
            mListener = (OnDilemmaFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDilemmaFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamDilemmaId = getArguments().getString(ARG_DILEMMA_ID);
        } else {
            throw new NullPointerException("ARG_DILEMMA_ID is empty in " + DilemmasFragment.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dilemma, container, false);
        mContext = getActivity();

        mDilemmaTextView = (TextView) view.findViewById(R.id.tv_dilemma);
        mResultView = (ResultView) view.findViewById(R.id.resultView);
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_activity_dilemma);

        mArgumentListAdapterPros = new ArgumentListAdapter(mContext, this);
        mArgumentListRecyclerViewPros = (RecyclerView) view.findViewById(R.id.rv_argument_pros);
        mArgumentListRecyclerViewPros.setLayoutManager(new LinearLayoutManager(mContext));
        mArgumentListRecyclerViewPros.setHasFixedSize(true);
        mArgumentListRecyclerViewPros.setAdapter(mArgumentListAdapterPros);

        mArgumentListAdapterCons = new ArgumentListAdapter(mContext, this);
        mArgumentListRecyclerViewCons = (RecyclerView) view.findViewById(R.id.rv_argument_cons);
        mArgumentListRecyclerViewCons.setLayoutManager(new LinearLayoutManager(mContext));
        mArgumentListRecyclerViewCons.setHasFixedSize(true);
        mArgumentListRecyclerViewCons.setAdapter(mArgumentListAdapterCons);

        mDilemmaTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDilemmaDescription(mParamDilemmaId, mDilemmaTextView.getText().toString() );
            }
        });

        mButtonAddArgument = (Button) view.findViewById(R.id.b_add_argument);
        mButtonAddArgument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAddArgument();
            }
        });

        mPresenter = new DilemmaPresenter(this, mContext, getLoaderManager());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mPresenter.loadData(mParamDilemmaId);
    }

    //region DilemmaContract.View implementation
    @Override
    public void onDilemmaMarkedAsDeleted(String dilemmaId) {
        mListener.onDilemmaMarkedAsDeleted(dilemmaId);
    }

    @Override
    public void onDilemmaMarkedAsAvailable(String dilemmaId) {
        mListener.onDilemmaMarkedAsAvailable(dilemmaId);
    }

    @Override
    public void setProsAndConsPercent(float prosPercent, float consPercent) {
        mResultView.setProsAndConsPercent(prosPercent, consPercent);
    }

    @Override
    public void setDilemmaDescription(String description) {
        mDilemmaTextView.setText(description);
    }

    @Override
    public void setProsArguments(Cursor cursor) {
        mArgumentListAdapterPros.swapCursor(cursor);
    }

    @Override
    public void setConsArguments(Cursor cursor) {
        mArgumentListAdapterCons.swapCursor(cursor);
    }

    @Override
    public void showMessage(String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showLoadingProcess() {
        mProgressBar.setVisibility(View.VISIBLE);
        mDilemmaTextView.setVisibility(View.INVISIBLE);
        mResultView.setVisibility(View.INVISIBLE);
        mButtonAddArgument.setVisibility(View.INVISIBLE);
        mArgumentListRecyclerViewPros.setVisibility(View.INVISIBLE);
        mArgumentListRecyclerViewCons.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoadingProcess() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mDilemmaTextView.setVisibility(View.VISIBLE);
        mResultView.setVisibility(View.VISIBLE);
        mButtonAddArgument.setVisibility(View.VISIBLE);
        mArgumentListRecyclerViewPros.setVisibility(View.VISIBLE);
        mArgumentListRecyclerViewCons.setVisibility(View.VISIBLE);
    }
    //endregion

    @Override
    public void onArgumentClick(long argumentId) {
        mListener.onClickEditArgument(mParamDilemmaId, String.valueOf(argumentId));
    }

    public void onClickAddArgument() {
        mListener.onClickAddArgument(mParamDilemmaId);
    }

    public void editDilemmaDescription(String dilemmaId, String currentDescription) {
        mListener.onClickEditDilemmaDescription(dilemmaId, currentDescription);
    }

    public void markDilemmaAsAvailable() {
        mPresenter.markDilemmaAsAvailable();
    }

    public void markDilemmaAsDeleted() {
        mPresenter.markDilemmaAsDeleted();
    }

    public void shareDilemma() {
//      Текст которым делятся выглядит так:
//
//      Дилемма: Купить ли велик мне?
//
//      Аргументы за: круто(1), полезно(8).
//
//      Аргументы против: не круто(8), не полезно(9).
//
//      Итог: За - 51%, Против - 49%.
        String textForShare;
        Resources res = getResources();
        List<ArgumentListAdapter.ArgumentItem> prosList = mArgumentListAdapterPros.getAllItems();
        List<ArgumentListAdapter.ArgumentItem> consList = mArgumentListAdapterCons.getAllItems();

        textForShare =
                res.getText(R.string.share_dilemma_section_name) +
                ": " +
                mDilemmaTextView.getText() + "\n\n";

        //region Add pros arguments info
        textForShare +=
                res.getText(R.string.share_pros_section_name) +
                ": ";

        if (prosList.size() == 0) {
            textForShare +=
                    res.getString(R.string.share_pros_empty_text);
        } else {
            textForShare +=
                    prosList.get(0).getDescription() +
                    "(" + prosList.get(0).getImportance() + ")";

            for (int i = 1; i < prosList.size(); i++) {
                textForShare +=
                        ", " + prosList.get(i).getDescription() +
                        "(" + prosList.get(0).getImportance() + ")";
            }
        }
        textForShare += ".\n\n";
        //endregion

        //region Add cons arguments info
        textForShare +=
                res.getText(R.string.share_cons_section_name) +
                        ": ";

        if (consList.size() == 0) {
            textForShare +=
                    res.getString(R.string.share_cons_empty_text);
        } else {
            textForShare +=
                    consList.get(0).getDescription() +
                            "(" + consList.get(0).getImportance() + ")";

            for (int i = 1; i < consList.size(); i++) {
                textForShare +=
                        ", " + consList.get(i).getDescription() +
                                "(" + consList.get(0).getImportance() + ")";
            }
        }
        textForShare += ".\n\n";
        //endregion

        textForShare +=
                res.getText(R.string.share_result_section_name) +
                ": " +
                res.getText(R.string.share_pros_text) + " - " + mResultView.getProsPercent() +  "%, " +
                res.getText(R.string.share_cons_text) + " - " + mResultView.getConsPercent() + "%.";


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textForShare);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_via_title)));
    }
}
