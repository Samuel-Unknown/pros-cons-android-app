package com.samuelunknown.proscons.dilemma;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.samuelunknown.proscons.edit_argument.EditArgumentActivity;
import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.edit_dilemma.EditDilemmaActivity;
import com.samuelunknown.proscons.utilities.PreferenceUtils;
import com.samuelunknown.proscons.utilities.StatusBarUtils;
import com.samuelunknown.proscons.utilities.ToolBarUtils;

public class DilemmaActivity
        extends AppCompatActivity
        implements DilemmaFragment.OnDilemmaFragmentListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = DilemmaActivity.class.getSimpleName();

    /**
     *  ключ для вытаскивания информации о ID дилеммы
     */
    public static final String INTENT_DILEMMA_ID_KEY = "DilemmaId";

    /**
     *  ключ для вытаскивания информации о том, является ли эта дилемма удалённой,
     *  т.е. открытой из раздела корзина
     */
    public static final String INTENT_DELETED_KEY = "Deleted";

    /**
     * является ли эта дилемма удалённой, т.е. открытой из раздела корзина
     */
    private boolean mIsDeleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceUtils.setActivityTheme(this, this, true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dilemma);

        //region Intent
        Intent intentThatStartedThisActivity = getIntent();

        if (intentThatStartedThisActivity != null) {
            if (intentThatStartedThisActivity.hasExtra(INTENT_DILEMMA_ID_KEY)) {
                String dilemmaIdString = intentThatStartedThisActivity.getStringExtra(INTENT_DILEMMA_ID_KEY);
                addDilemmaFragment(dilemmaIdString);
            } else {
                throw new NullPointerException("Intent hasn't INTENT_DILEMMA_ID_KEY in " + DilemmaActivity.class);
            }

            if (intentThatStartedThisActivity.hasExtra(INTENT_DELETED_KEY)) {
                mIsDeleted = intentThatStartedThisActivity.getBooleanExtra(INTENT_DELETED_KEY, false);
            }

        } else {
            throw new NullPointerException("Intent is null in " + DilemmaActivity.class);
        }
        //endregion

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.dilemma_activity_toolbar);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region Toolbar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion
    }

    //region Menu Options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dilemma_activity_menu, menu);

        menu.setGroupVisible(R.id.group_dilemma_activity_common, true);
        menu.setGroupVisible(R.id.group_dilemma_activity_available_dilemma, !mIsDeleted);
        menu.setGroupVisible(R.id.group_dilemma_activity_deleted_dilemma, mIsDeleted);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        DilemmaFragment fragment =
                (DilemmaFragment) getSupportFragmentManager().
                        findFragmentById(R.id.dilemma_fragment_container);

        switch (itemThatWasClickedId) {
            case R.id.action_share:
                if (fragment != null) {
                    fragment.shareDilemma();
                }
                return true;
            case R.id.action_delete_dilemma:
                if (fragment != null) {
                    fragment.markDilemmaAsDeleted();
                }
                return true;
            case R.id.action_restore_dilemmas:
                if (fragment != null) {
                    fragment.markDilemmaAsAvailable();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //endregion

    //region DilemmaFragment Handler
    @Override
    public void onClickAddArgument(String dilemmaId) {
        startEditArgumentActivity(this, dilemmaId, null);
    }

    @Override
    public void onClickEditArgument(String dilemmaId, String argumentId) {
        startEditArgumentActivity(this, dilemmaId, argumentId);
    }

    @Override
    public void onClickEditDilemmaDescription(String dilemmaId, String currentDescription) {
        startEditDilemmaActivity(this, dilemmaId, currentDescription);
    }

    @Override
    public void onDilemmaMarkedAsAvailable(String dilemmaId) {
        //TODO надо дать возможность отменить опреацию удаления дилеммы,
        // как делается когда удаление происходит из списка дилемм
        finish();
    }

    @Override
    public void onDilemmaMarkedAsDeleted(String dilemmaId) {
        //TODO потом добавить snackbar для отмены операции
        finish();
    }
    //endregion

    private void addDilemmaFragment(String dilemmaId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DilemmaFragment dilemmaFragment =
                (DilemmaFragment) fragmentManager.findFragmentById(R.id.dilemma_fragment_container);

        if (dilemmaFragment == null) {
            fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.dilemma_fragment_container, DilemmaFragment.newInstance(dilemmaId))
                .commit();
        } else {
            // P.S. при повороте телефона в режим landscape и обратно
            // fragment отсоединяется от контейнера, но так же автоматом и аттачится туда,
            // и поэтому по идее код этого условия не выполняется.. но я не уверен везде ли он
            // автоматом аттачится, так что пусть пока будет.
            if (dilemmaFragment.isDetached()) {
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .attach(dilemmaFragment)
                        .commit();
            }
        }
    }

    private void startEditDilemmaActivity(@NonNull Context context, String dilemmaId, String currentDescription) {
        Intent intent = new Intent(context, EditDilemmaActivity.class);
        intent.putExtra(EditDilemmaActivity.INTENT_DILEMMA_ID_KEY, dilemmaId);
        intent.putExtra(EditDilemmaActivity.INTENT_DILEMMA_DESCRIPTION_KEY, currentDescription);
        startActivity(intent);
    }

    private void startEditArgumentActivity(@NonNull Context context, String dilemmaId, String argumentId) {
        Intent intent = new Intent(context, EditArgumentActivity.class);
        intent.putExtra(EditArgumentActivity.INTENT_DILEMMA_ID_KEY, dilemmaId);
        intent.putExtra(EditArgumentActivity.INTENT_ARGUMENT_ID_KEY, argumentId);
        startActivity(intent);
    }
}
