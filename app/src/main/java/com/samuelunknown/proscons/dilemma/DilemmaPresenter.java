package com.samuelunknown.proscons.dilemma;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.samuelunknown.proscons.data.DbContract;

import java.lang.ref.WeakReference;

public class DilemmaPresenter implements
        DilemmaContract.Presenter,
        LoaderManager.LoaderCallbacks<Cursor> {

    //region Const Values
    private static final String TAG = DilemmaPresenter.class.getSimpleName();

    //region DILEMMA_PROJECTION
    private static final String[] DILEMMA_PROJECTION = {
            DbContract.DilemmaEntry.COLUMN_DESCRIPTION,
            DbContract.DilemmaEntry.COLUMN_PROS_PERCENT,
            DbContract.DilemmaEntry.COLUMN_CONS_PERCENT
    };

    // Если порядок строк в массиве DILEMMA_PROJECTION изменится,
    // следует изменить и эти индексы
    private static final int INDEX_DILEMMA_DESCRIPTION = 0;
    private static final int INDEX_DILEMMA_PROS_PERCENT = 1;
    private static final int INDEX_DILEMMA_CONS_PERCENT = 2;
    //endregion

    //region ARGUMENT_PROJECTION
    public static final String[] ARGUMENT_PROJECTION = {
            DbContract.ArgumentEntry._ID,
            DbContract.ArgumentEntry.COLUMN_DESCRIPTION,
            DbContract.ArgumentEntry.COLUMN_POSITIVE,
            DbContract.ArgumentEntry.COLUMN_IMPORTANCE,
            DbContract.ArgumentEntry.COLUMN_TIMESTAMP
    };

    // Если порядок строк в массиве ARGUMENT_PROJECTION изменится,
    // следует изменить и эти индексы
    public static final int INDEX_ARGUMENT_ID = 0;
    public static final int INDEX_ARGUMENT_DESCRIPTION = 1;
    public static final int INDEX_ARGUMENT_POSITIVE = 2;
    public static final int INDEX_ARGUMENT_IMPORTANCE = 3;
    public static final int INDEX_ARGUMENT_TIMESTAMP = 4;
    //endregion

    //region LOADERS ID
    // Уникальные ID для загрузчика данных из БД
    private static final int DILEMMA_LOADER_ID = 1;
    private static final int ARGUMENT_PROS_LOADER_ID = 2;
    private static final int ARGUMENT_CONS_LOADER_ID = 3;
    //endregion

    //region TOKENS
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED = 111;
    private static final int UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE= 222;
    //endregion
    //endregion

    //region Variables
    private DilemmaContract.View mView;
    private Context mContext;
    private LoaderManager mLoaderManager;
    private DilemmaAsyncQueryHandler mAsyncQueryHandler;
    private String mDilemmaId;
    //endregion

    public DilemmaPresenter(@NonNull DilemmaFragment view, @NonNull Context context,
                            @NonNull LoaderManager loaderManager) {
        mView = view;
        mContext = context;
        mLoaderManager = loaderManager;
        mAsyncQueryHandler = new DilemmaAsyncQueryHandler(this, context.getContentResolver());
    }

    //region Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case DILEMMA_LOADER_ID:
                Uri uri = DbContract.DilemmaEntry.CONTENT_URI;
                // uri на конкретную дилемму
                uri = uri.buildUpon().appendPath(mDilemmaId).build();

                return new CursorLoader(mContext,
                        uri,
                        DILEMMA_PROJECTION,
                        null,
                        null,
                        null);

            case ARGUMENT_PROS_LOADER_ID:
                return new CursorLoader(mContext,
                        DbContract.ArgumentEntry.CONTENT_URI,
                        ARGUMENT_PROJECTION,
                        DbContract.ArgumentEntry.COLUMN_ID_DILEMMA + "=? AND " +
                                 DbContract.ArgumentEntry.COLUMN_POSITIVE + "=?",
                        new String[] {mDilemmaId, DbContract.ArgumentEntry.POSITIVE_VALUE},
                        DbContract.ArgumentEntry.COLUMN_IMPORTANCE + " DESC");

            case ARGUMENT_CONS_LOADER_ID:
                return new CursorLoader(mContext,
                        DbContract.ArgumentEntry.CONTENT_URI,
                        ARGUMENT_PROJECTION,
                        DbContract.ArgumentEntry.COLUMN_ID_DILEMMA + "=? AND " +
                                 DbContract.ArgumentEntry.COLUMN_POSITIVE + "=?",
                        new String[] {mDilemmaId, DbContract.ArgumentEntry.NEGATIVE_VALUE},
                        DbContract.ArgumentEntry.COLUMN_IMPORTANCE + " DESC");
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId()) {
            case DILEMMA_LOADER_ID:
                if (data.moveToNext()) {
                    String description = data.getString(INDEX_DILEMMA_DESCRIPTION);
                    String prosPercent = data.getString(INDEX_DILEMMA_PROS_PERCENT);
                    String consPercent = data.getString(INDEX_DILEMMA_CONS_PERCENT);

                    float prosPercentValue = Float.valueOf(prosPercent);
                    float consPercentValue = Float.valueOf(consPercent);

                    mView.setProsAndConsPercent(prosPercentValue, consPercentValue);
                    mView.setDilemmaDescription(description);
                } else {
                    throw new NullPointerException("Dilemma data is null in " + DilemmaPresenter.class);
                }
                mView.hideLoadingProcess();
                break;
            case ARGUMENT_PROS_LOADER_ID:
                mView.setProsArguments(data);
                break;
            case ARGUMENT_CONS_LOADER_ID:
                mView.setConsArguments(data);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case DILEMMA_LOADER_ID:
                mView.setDilemmaDescription("");
                break;
            case ARGUMENT_PROS_LOADER_ID:
                mView.setProsArguments(null);
                break;
            case ARGUMENT_CONS_LOADER_ID:
                mView.setConsArguments(null);
                break;
        }
    }
    //endregion

    @Override
    public void markDilemmaAsAvailable() {
        updateDilemmasCurrentState(UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE);
    }

    @Override
    public void markDilemmaAsDeleted() {
        updateDilemmasCurrentState(
                UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED,
                DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED);
    }

    @Override
    public void loadData(String dilemmaId) {
        mView.showLoadingProcess();

        mDilemmaId = dilemmaId;

        startLoader(DILEMMA_LOADER_ID);
        startLoader(ARGUMENT_PROS_LOADER_ID);
        startLoader(ARGUMENT_CONS_LOADER_ID);
    }

    private void startLoader(int loaderId) {
        Loader loader = mLoaderManager.getLoader(loaderId);
        if (loader != null) {
            mLoaderManager.restartLoader(loaderId, null, this);
        } else {
            mLoaderManager.initLoader(loaderId, null, this);
        }
    }

    private void updateDilemmasCurrentState(int updateToken, String newState) {

        if (!newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_ARCHIVED) &&
                !newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_AVAILABLE) &&
                !newState.equals(DbContract.DilemmaEntry.VALUE_CURRENT_STATE_DELETED)) {
            throw new UnsupportedOperationException("updateSelectedDilemmasCurrentState(): unsupported newState -> " + newState);
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(
                DbContract.DilemmaEntry.COLUMN_CURRENT_STATE,
                newState);

        mAsyncQueryHandler.startUpdate(
                updateToken,
                null,
                DbContract.DilemmaEntry.CONTENT_URI,
                contentValues,
                DbContract.DilemmaEntry._ID + " =?",
                new String[]{mDilemmaId});
    }

    private void onUpdateCompleted(int token, int updatedRows) {
        switch (token) {
            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED
            case UPDATE_DILEMMA_TOKEN_MARK_AS_DELETED: {
                if (updatedRows > 0) {
                    mView.onDilemmaMarkedAsDeleted(mDilemmaId);
                } else {
                    mView.showMessage("Error: dilemma didn't mark as deleted");
                }
                break;
            }
            //endregion

            //region case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE
            case UPDATE_DILEMMA_TOKEN_MARK_AS_AVAILABLE: {
                if (updatedRows > 0) {
                    mView.onDilemmaMarkedAsAvailable(mDilemmaId);
                } else {
                    mView.showMessage("Error: dilemma didn't mark as available");
                }
                break;
            }
            //endregion
        }
    }

    private static class DilemmaAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<DilemmaPresenter> mPresenterRef;

        public DilemmaAsyncQueryHandler(DilemmaPresenter presenter, ContentResolver cr) {
            super(cr);

            // берём weak reference что бы потом по завершении операций вставки,
            // обновления или удаления можно было вызвать методы класса DilemmaPresenter,
            // если его экземпляр ещё не был удалён
            mPresenterRef = new WeakReference<>(presenter);
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);

            DilemmaPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onUpdateCompleted(token, result);
            }
        }
    }
}
