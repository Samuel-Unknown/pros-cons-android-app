package com.samuelunknown.proscons.edit_argument;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.data.DbContract;

public class EditArgumentFragment
        extends Fragment
        implements EditArgumentContract.View, EditArgumentContract.FragmentView {

    private static final String TAG = EditArgumentFragment.class.getSimpleName();

    //region Variables
    private EditText mArgumentDescriptionEditText;
    private RadioGroup mRadioGroup;
    private SeekBar mSeekBar;
    private TextView mArgumentImportanceTextView;
    private ProgressBar mProgressBar;

    private EditArgumentPresenter mPresenter;
    private EditArgumentContract.FragmentListener mListener;
    //endregion

    //region Arguments
    private static final String ARG_DILEMMA_ID = "dilemmaId";
    private static final String ARG_ARGUMENT_ID = "argumentId";

    private String mParamDilemmaId;
    private String mParamArgumentId;
    //endregion

    //region Create instance of EditArgumentFragment
    public EditArgumentFragment() {
        // Required empty public constructor
    }

    public static EditArgumentFragment newInstance(String dilemmaId, String argumentId) {
        EditArgumentFragment fragment = new EditArgumentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DILEMMA_ID, dilemmaId);
        args.putString(ARG_ARGUMENT_ID, argumentId);
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EditArgumentContract.FragmentListener) {
            mListener = (EditArgumentContract.FragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditArgumentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamDilemmaId = getArguments().getString(ARG_DILEMMA_ID);
            mParamArgumentId = getArguments().getString(ARG_ARGUMENT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_edit_argument, container, false);
        final Context context = getActivity();

        //region Init Variables
        mArgumentDescriptionEditText = (EditText) view.findViewById(R.id.et_argument_description);
        mArgumentImportanceTextView = (TextView) view.findViewById(R.id.tv_argument_importance);
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_argument_load_progress);
        mRadioGroup = (RadioGroup) view.findViewById(R.id.rg_argument_pro_con);

        mSeekBar = (SeekBar) view.findViewById(R.id.sb_argument_importance);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mArgumentImportanceTextView.setText(String.valueOf(progress + 1));
            }
        });

        Button saveArgumentButton = (Button) view.findViewById(R.id.b_save_argument);
        saveArgumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSaveArgument();
            }
        });
        //endregion

        mPresenter = new EditArgumentPresenter(this, context, getLoaderManager(),
                mParamDilemmaId, mParamArgumentId);

        if (mParamArgumentId == null) {
            // Создание нового аргумента
            mSeekBar.setProgress(4);
        } else {
            // Редактирование выбранного аргумента
            mPresenter.loadData();
        }

        return view;
    }

    //region RadioGroup
    private int getRadioGroupValue() {
        switch (mRadioGroup.getCheckedRadioButtonId()) {
            case R.id.rb_pros:
                return Integer.valueOf(DbContract.ArgumentEntry.POSITIVE_VALUE);
            case R.id.rb_cons:
                return Integer.valueOf(DbContract.ArgumentEntry.NEGATIVE_VALUE);
            default:
                return Integer.valueOf(DbContract.ArgumentEntry.NEGATIVE_VALUE);
        }
    }

    /**
     * Устанавливает RadioGroup таким что аргумент либо ЗА, либо ПРОТИВ
     * @param value может принимать только два аргумента,
     *              либо DbContract.ArgumentEntry.POSITIVE_VALUE,
     *              либо DbContract.ArgumentEntry.NEGATIVE_VALUE
     */
    private void setRadioGroupValue(String value) {
        switch (value) {
            case DbContract.ArgumentEntry.POSITIVE_VALUE:
                mRadioGroup.check(R.id.rb_pros);
                break;
            case DbContract.ArgumentEntry.NEGATIVE_VALUE:
                mRadioGroup.check(R.id.rb_cons);
                break;
            default:
                Log.w(TAG, "setRadioGroupValue() wrong argument");
        }
    }
    //endregion

    public void onClickSaveArgument() {
        String description = mArgumentDescriptionEditText.getText().toString();

        if (mParamArgumentId == null) {
            // Создание нового аргумента
            mPresenter.addNewArgument(description,
                    getRadioGroupValue(), mSeekBar.getProgress() + 1);
        } else {
            // Редактирование выбранного аргумента
            mPresenter.updateArgument(description,
                    getRadioGroupValue(), mSeekBar.getProgress() + 1);
        }
    }

    //region EditArgumentContract.View implementation
    @Override
    public void showLoadingProcess() {
        mProgressBar.setVisibility(View.VISIBLE);
        mArgumentDescriptionEditText.setVisibility(View.INVISIBLE);
        mRadioGroup.setVisibility(View.INVISIBLE);
        mSeekBar.setVisibility(View.INVISIBLE);
        mArgumentImportanceTextView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoadingProcess() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mArgumentDescriptionEditText.setVisibility(View.VISIBLE);
        mRadioGroup.setVisibility(View.VISIBLE);
        mSeekBar.setVisibility(View.VISIBLE);
        mArgumentImportanceTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setArgumentDescription(String description) {
        mArgumentDescriptionEditText.setText(description);
    }

    @Override
    public void setArgumentPositive(int positive) {
        setRadioGroupValue(String.valueOf(positive));
    }

    @Override
    public void setArgumentImportance(int importance) {
        mSeekBar.setProgress(importance - 1);
        mArgumentImportanceTextView.setText(String.valueOf(importance));
    }

    @Override
    public void onEditArgumentFinish() {
        mListener.onEditArgumentFinish();
    }
    //endregion

    //region EditArgumentContract.FragmentView implementation
    @Override
    public void deleteArgument() {
        mPresenter.deleteArgument();
    }
    //endregion
}
