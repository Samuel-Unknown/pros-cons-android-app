package com.samuelunknown.proscons.edit_argument;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.utilities.PreferenceUtils;
import com.samuelunknown.proscons.utilities.StatusBarUtils;
import com.samuelunknown.proscons.utilities.ToolBarUtils;

public class EditArgumentActivity
        extends AppCompatActivity
        implements EditArgumentContract.FragmentListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = EditArgumentActivity.class.getSimpleName();

    private EditArgumentContract.FragmentView mFragmentView;

    //region Arguments
    public static final String INTENT_DILEMMA_ID_KEY = "DilemmaId";
    public static final String INTENT_ARGUMENT_ID_KEY = "ArgumentId";

    private String mDilemmaId;
    private String mArgumentId;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceUtils.setActivityTheme(this, this, true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_argument);

        //region Check intentThatStartedThisActivity != null
        Intent intentThatStartedThisActivity = getIntent();

        if (intentThatStartedThisActivity != null) {

            Bundle extras = getIntent().getExtras();

            if (extras == null) {
                throw new NullPointerException("Extras is null in " + EditArgumentActivity.class);
            }

            if ( extras.getString(INTENT_ARGUMENT_ID_KEY) != null ) {
                mArgumentId = intentThatStartedThisActivity.getStringExtra(INTENT_ARGUMENT_ID_KEY);
            }

            if ( extras.getString(INTENT_DILEMMA_ID_KEY) != null ) {
                mDilemmaId = intentThatStartedThisActivity.getStringExtra(INTENT_DILEMMA_ID_KEY);
            }

            if (mArgumentId == null && mDilemmaId == null)  {
                throw new NullPointerException("Intent hasn't EXTRA in " + EditArgumentActivity.class);
            }
        } else {
            throw new NullPointerException("Intent is null in " + this.getClass());
        }
        //endregion

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.edit_argument_activity_toolbar);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region Toolbar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        mFragmentView = addEditArgumentFragment(mDilemmaId, mArgumentId);
    }

    //region Menu Options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_argument_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        switch (itemThatWasClickedId) {
            case R.id.action_delete_argument:

                // если этот аргумент существует в БД (т.е. в этом активити не создаётся новый, а редактируется старый)
                if (mArgumentId != null) {
                    // удаляем его из БД
                    mFragmentView.deleteArgument();
                } else {
                    finish();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //endregion

    private EditArgumentFragment addEditArgumentFragment(String dilemmaId, String argumentId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditArgumentFragment editArgumentFragment =
                (EditArgumentFragment) fragmentManager.findFragmentById(R.id.edit_argument_fragment_container);

        if (editArgumentFragment == null) {
            editArgumentFragment = EditArgumentFragment.newInstance(dilemmaId, argumentId);

            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.edit_argument_fragment_container, editArgumentFragment)
                    .commit();
        } else {
            // P.S. при повороте телефона в режим landscape и обратно
            // fragment отсоединяется от контейнера, но так же автоматом и аттачится туда,
            // и поэтому по идее код этого условия не выполняется.. но я не уверен везде ли он
            // автоматом аттачится, так что пусть пока будет.
            if (editArgumentFragment.isDetached()) {
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .attach(editArgumentFragment)
                        .commit();
            }
        }

        return editArgumentFragment;
    }

    @Override
    public void onEditArgumentFinish() {
        finish();
    }
}
