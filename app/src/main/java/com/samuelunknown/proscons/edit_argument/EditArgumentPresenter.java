package com.samuelunknown.proscons.edit_argument;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.samuelunknown.proscons.data.DbContract;
import com.samuelunknown.proscons.utilities.AppDateUtils;

import java.lang.ref.WeakReference;
import java.util.Locale;

//region About Algorithm
// Есть два 3 момента когда может измениться процентное соотношение всех ЗА и ПРОТИВ:
// 1) Вставка нового аргумента
// 2) Удаление одного из аргументов
// 3) Редактирование аргумента (изменится может как вес, так и то за какую сторону
//    выступает аргумент ЗА или ПРОТИВ)

// После записи в БД этих изменений (отслеживаем по колбэку) происходит следующее:
// 1) Загружаются данные с суммами весов всех ЗА и ПРОТИВ
// 2) Считаются в процентоном соотношении реультат голосования ЗА и ПРОТИВ
// 3) Обновляются проценты полученные сумарно ЗА и ПРОТИВ в таблице дилемы БД
// 4) Отправляется сообщение о конце редактирования, которое ловит Fragment и такое же сообщение
//      Activity, а она уже в свою очередь просто завершает работу.
//endregion

public class EditArgumentPresenter
        implements EditArgumentContract.Presenter,
        LoaderManager.LoaderCallbacks<Cursor> {

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = EditArgumentPresenter.class.getSimpleName();

    //region ARGUMENT_PROJECTION
    private static final String[] ARGUMENT_PROJECTION = {
            DbContract.ArgumentEntry.COLUMN_DESCRIPTION,
            DbContract.ArgumentEntry.COLUMN_POSITIVE,
            DbContract.ArgumentEntry.COLUMN_IMPORTANCE
    };

    // Если порядок строк в массиве ARGUMENT_PROJECTION изменится,
    // следует изменить и эти индексы
    private static final int INDEX_ARGUMENT_DESCRIPTION = 0;
    private static final int INDEX_ARGUMENT_POSITIVE = 1;
    private static final int INDEX_ARGUMENT_IMPORTANCE = 2;
    //endregion

    //region LOADERS ID
    // Уникальный ID для загрузчика данных из БД (загрузка аргумента)
    private static final int ARGUMENT_LOADER_ID = 1;
    // Уникальный ID для загрузчика данных из БД (загрузка суммарных значений положительных и
    // отрицательных весов для связанной с этим аргументом дилеммой)
    private static final int PROS_CONS_TOTAL_VALUES_LOADER_ID = 2;
    //endregion

    //region TOKENS
    private static final int INSERT_ARGUMENT_TOKEN = 10;
    private static final int UPDATE_ARGUMENT_TOKEN = 11;
    private static final int DELETE_ARGUMENT_TOKEN = 12;
    private static final int UPDATE_DILEMMA_TOKEN = 13;
    //endregion

    //region PROS_CONS_TOTAL_VALUES_PROJECTION
    private static final String[] PROS_CONS_TOTAL_VALUES_PROJECTION = {
            DbContract.COLUMN_TOTAL_POSITIVE,
            DbContract.COLUMN_TOTAL_NEGATIVE
    };

    // Если порядок строк в массиве PROS_CONS_TOTAL_VALUES_PROJECTION изменится,
    // следует изменить и эти индексы
    private static final int INDEX_TOTAL_POSITIVE = 0;
    private static final int INDEX_TOTAL_NEGATIVE = 1;
    //endregion
    //endregion

    //region Variables
    private EditArgumentContract.View mView;
    private Context mContext;
    private LoaderManager mLoaderManager;
    private EditArgumentAsyncQueryHandler mAsyncQueryHandler;
    private String mDilemmaId;
    private String mArgumentId;
    //endregion

    EditArgumentPresenter(@NonNull EditArgumentFragment view, @NonNull Context context,
                                 @NonNull LoaderManager loaderManager,
                                 @NonNull String dilemmaId, @Nullable String argumentId) {
        mView = view;
        mContext = context;
        mDilemmaId = dilemmaId;
        mArgumentId = argumentId;
        mLoaderManager = loaderManager;
        mAsyncQueryHandler = new EditArgumentAsyncQueryHandler(this, context.getContentResolver());
    }

    //region Calculate Percent
    private float getProsPercent(int prosValue, int consValue) {
        int total = prosValue + consValue;

        if (total == 0)
            return 50;

        return 100f * prosValue / total;
    }

    private float getConsPercent(int prosValue, int consValue) {
        int total = prosValue + consValue;

        if (total == 0)
            return 50;

        return 100f * consValue / total;
    }
    //endregion

    private void loadArgumentData() {
        startLoader(ARGUMENT_LOADER_ID);
    }

    private void loadProsConsTotalValues() {
        startLoader(PROS_CONS_TOTAL_VALUES_LOADER_ID);
    }

    private void startLoader(int loaderId) {
        Loader loader = mLoaderManager.getLoader(loaderId);
        if (loader == null) {
            mLoaderManager.initLoader(loaderId, null, this);
        } else {
            mLoaderManager.restartLoader(loaderId, null, this);
        }
    }

    private void updateProsConsValuesInDilemma(String dilemmaId, float prosPercent, float consPercent) {
        Uri uri = DbContract.DilemmaEntry.CONTENT_URI;
        uri = uri.buildUpon().appendPath(dilemmaId).build();

        ContentValues contentValues = new ContentValues();
        // Использую Locale.ENGLISH что бы в БД формат числа был задан через точку,
        // например так -> "15.0" будут записаны 15%
        contentValues.put(DbContract.DilemmaEntry.COLUMN_PROS_PERCENT,
                String.format(Locale.ENGLISH, "%.1f", prosPercent));
        contentValues.put(DbContract.DilemmaEntry.COLUMN_CONS_PERCENT,
                String.format(Locale.ENGLISH, "%.1f", consPercent));
        contentValues.put(DbContract.DilemmaEntry.COLUMN_TIMESTAMP,
                AppDateUtils.getGmtForCurrentTime());

        mAsyncQueryHandler.startUpdate(UPDATE_DILEMMA_TOKEN, null, uri,
                contentValues, null, null);
    }

    //region Cursor Loader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case ARGUMENT_LOADER_ID:

                Uri uriArg = DbContract.ArgumentEntry.CONTENT_URI;
                uriArg = uriArg.buildUpon().appendPath(mArgumentId).build();

                return new CursorLoader(
                        mContext, uriArg, ARGUMENT_PROJECTION,
                        null, null, null);

            case PROS_CONS_TOTAL_VALUES_LOADER_ID:

                Uri uriTotal = DbContract.PROS_CONS_TOTAL_VALUES_CONTENT_URI;
                uriTotal = uriTotal.buildUpon().appendPath(mDilemmaId).build();

                return new CursorLoader(
                        mContext, uriTotal, PROS_CONS_TOTAL_VALUES_PROJECTION,
                        null, null, null);

            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case ARGUMENT_LOADER_ID:
                if (data.moveToNext()) {
                    String description = data.getString(INDEX_ARGUMENT_DESCRIPTION);
                    mView.setArgumentDescription(description);

                    int positive = data.getInt(INDEX_ARGUMENT_POSITIVE);
                    mView.setArgumentPositive(positive);

                    int importance = data.getInt(INDEX_ARGUMENT_IMPORTANCE);
                    mView.setArgumentImportance(importance);

                } else {
                    throw new NullPointerException("Argument data is null in " +
                            EditArgumentPresenter.class);
                }

                mView.hideLoadingProcess();
                mLoaderManager.destroyLoader(ARGUMENT_LOADER_ID);
                break;
            case PROS_CONS_TOTAL_VALUES_LOADER_ID:
                if (data.moveToNext()) {

                    int totalPositive = data.getInt(INDEX_TOTAL_POSITIVE);
                    int totalNegative = data.getInt(INDEX_TOTAL_NEGATIVE);

                    float prosPercent = getProsPercent(totalPositive, totalNegative);
                    float consPercent = getConsPercent(totalPositive, totalNegative);

                    updateProsConsValuesInDilemma(mDilemmaId, prosPercent, consPercent);
                } else {
                    throw new NullPointerException("Pros & cons total values data is null in " +
                            EditArgumentPresenter.class);
                }
                mLoaderManager.destroyLoader(PROS_CONS_TOTAL_VALUES_LOADER_ID);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
    //endregion

    //region EditArgumentContract.Presenter implementation
    @Override
    public void loadData() {
        mView.showLoadingProcess();
        loadArgumentData();
    }

    @Override
    public void addNewArgument(String argumentDescription,
                               int positive, int importance) {

        if (argumentDescription.length() == 0) {
            return;
        }

        ContentValues contentValues = new ContentValues();

        // Заполняем только поле с описанием дилеммы, для остальных полей
        contentValues.put(DbContract.ArgumentEntry.COLUMN_ID_DILEMMA, Long.valueOf(mDilemmaId));
        contentValues.put(DbContract.ArgumentEntry.COLUMN_DESCRIPTION, argumentDescription);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_POSITIVE, positive);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_IMPORTANCE, importance);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_TIMESTAMP, AppDateUtils.getGmtForCurrentTime());

        mAsyncQueryHandler.startInsert(INSERT_ARGUMENT_TOKEN, null, DbContract.ArgumentEntry.CONTENT_URI, contentValues);
    }

    @Override
    public void updateArgument(String argumentDescription,
                               int positive, int importance) {

        if (argumentDescription.length() == 0) {
            return;
        }

        ContentValues contentValues = new ContentValues();

        contentValues.put(DbContract.ArgumentEntry.COLUMN_DESCRIPTION, argumentDescription);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_POSITIVE, positive);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_IMPORTANCE, importance);
        contentValues.put(DbContract.ArgumentEntry.COLUMN_TIMESTAMP, AppDateUtils.getGmtForCurrentTime());

        Uri uri = DbContract.ArgumentEntry.CONTENT_URI;
        uri = uri.buildUpon().appendPath(mArgumentId).build();

        mAsyncQueryHandler.startUpdate(UPDATE_ARGUMENT_TOKEN, null, uri,
                contentValues, null, null);
    }

    @Override
    public void deleteArgument() {
        Uri uri = DbContract.ArgumentEntry.CONTENT_URI;
        uri = uri.buildUpon().appendPath(mArgumentId).build();

        mAsyncQueryHandler.startDelete(DELETE_ARGUMENT_TOKEN, null, uri, null, null);
    }
    //endregion

    //region Insert Update Delete Completed
    private void onInsertCompleted(int token, Uri uri) {
        if (token == INSERT_ARGUMENT_TOKEN) {
            loadProsConsTotalValues();
        }
    }

    private void onUpdateCompleted(int token, int updatedRows) {
        switch (token) {
            case UPDATE_ARGUMENT_TOKEN: {
                loadProsConsTotalValues();
                break;
            }
            case UPDATE_DILEMMA_TOKEN: {
                mView.onEditArgumentFinish();
                break;
            }
        }
    }

    private void onDeleteCompleted(int token, int deletedRows) {
        if (token == DELETE_ARGUMENT_TOKEN) {
            loadProsConsTotalValues();
        }
    }
    //endregion

    private static class EditArgumentAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<EditArgumentPresenter> mPresenterRef;

        EditArgumentAsyncQueryHandler(EditArgumentPresenter presenter, ContentResolver cr) {
            super(cr);

            // берём weak reference что бы потом по завершении операций вставки,
            // обновления или удаления можно было вызвать методы класса EditArgumentPresenter,
            // если его экземпляр ещё не был удалён
            mPresenterRef = new WeakReference<>(presenter);
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            super.onInsertComplete(token, cookie, uri);

            EditArgumentPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onInsertCompleted(token, uri);
            }
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);

            EditArgumentPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onUpdateCompleted(token, result);
            }
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            super.onDeleteComplete(token, cookie, result);

            EditArgumentPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onDeleteCompleted(token, result);
            }
        }
    }
}