package com.samuelunknown.proscons.edit_argument;

public interface EditArgumentContract {
    interface View {
        void showLoadingProcess();
        void hideLoadingProcess();

        void setArgumentDescription(String description);
        void setArgumentPositive(int positive);
        void setArgumentImportance(int importance);

        void onEditArgumentFinish();
    }

    interface Presenter {
        void loadData();

        void addNewArgument(String argumentDescription,
                            int positive, int importance);
        void updateArgument(String argumentDescription,
                            int positive, int importance);
        void deleteArgument();
    }

    interface FragmentView {
        void deleteArgument();
    }

    interface FragmentListener {
        void onEditArgumentFinish();
    }
}
