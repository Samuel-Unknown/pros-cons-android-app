package com.samuelunknown.proscons.edit_dilemma;

public interface EditDilemmaContract {
    interface View {
        void onDilemmaUpdated(String dilemmaId);
        void onDilemmaInserted(String dilemmaId);
    }

    interface Presenter {
        void addNewDilemma(String dilemmaDescription);
        void updateDilemma(String dilemmaId, String dilemmaDescription);
    }
}
