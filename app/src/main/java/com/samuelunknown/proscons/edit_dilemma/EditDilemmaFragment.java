package com.samuelunknown.proscons.edit_dilemma;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.samuelunknown.proscons.MainActivity;
import com.samuelunknown.proscons.R;

public class EditDilemmaFragment extends Fragment implements EditDilemmaContract.View {

    interface OnEditDilemmaFragmentListener {
        void onDilemmaUpdated(String dilemmaId);
        void onDilemmaInserted(String dilemmaId);
    }

    //region Variables
    private EditText mDilemmaDescriptionEditText;

    private OnEditDilemmaFragmentListener mListener;
    private EditDilemmaContract.Presenter mPresenter;

    private static final String TAG = MainActivity.class.getSimpleName();
    //endregion

    //region Arguments
    private static final String ARG_DILEMMA_ID = "paramDilemmaID";
    private static final String ARG_DILEMMA_DESCRIPTION = "paramDilemmaDescription";

    private String mParamDilemmaId;
    private String mParamDilemmaDescription;
    //endregion

    //region Create instance of EditDilemmaFragment
    public EditDilemmaFragment() {
        // Required empty public constructor
    }

    public static EditDilemmaFragment newInstance(String paramDilemmaID, String paramDilemmaDescription) {
        EditDilemmaFragment fragment = new EditDilemmaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DILEMMA_ID, paramDilemmaID);
        args.putString(ARG_DILEMMA_DESCRIPTION, paramDilemmaDescription);
        fragment.setArguments(args);
        return fragment;
    }
    //endregion

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditDilemmaFragmentListener) {
            mListener = (OnEditDilemmaFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditDilemmaListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamDilemmaId = getArguments().getString(ARG_DILEMMA_ID);
            mParamDilemmaDescription = getArguments().getString(ARG_DILEMMA_DESCRIPTION);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_edit_dilemma, container, false);
        final Context context = getActivity();

        mDilemmaDescriptionEditText = (EditText) view.findViewById(R.id.et_dilemma);
        mDilemmaDescriptionEditText.setText(mParamDilemmaDescription);

        Button saveDilemmaButton = (Button) view.findViewById(R.id.b_save_dilemma);
        saveDilemmaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSaveDilemma();
            }
        });

        mPresenter = new EditDilemmaPresenter(this, context);

        return view;
    }

    @Override
    public void onDilemmaUpdated(String dilemmaId) {
        mListener.onDilemmaUpdated(dilemmaId);
    }

    @Override
    public void onDilemmaInserted(String dilemmaId) {
        mListener.onDilemmaInserted(dilemmaId);
    }

    private void onClickSaveDilemma() {
        String dilemmaDescription = mDilemmaDescriptionEditText.getText().toString();

        if (mParamDilemmaId != null) {
            mPresenter.updateDilemma(mParamDilemmaId, dilemmaDescription);
        } else {
            mPresenter.addNewDilemma(dilemmaDescription);
        }
    }
}
