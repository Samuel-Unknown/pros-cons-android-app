package com.samuelunknown.proscons.edit_dilemma;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.samuelunknown.proscons.data.DbContract;
import com.samuelunknown.proscons.utilities.AppDateUtils;

import java.lang.ref.WeakReference;

public class EditDilemmaPresenter implements EditDilemmaContract.Presenter {

    //region Variables
    private EditDilemmaContract.View mView;
    private EditDilemmaAsyncQueryHandler mAsyncQueryHandler;

    private String updatedDilemmaId;

    private static final int INSERT_DILEMMA_TOKEN = 10;
    private static final int UPDATE_DILEMMA_TOKEN = 11;

    private static final String TAG = EditDilemmaPresenter.class.getSimpleName();
    //endregion

    public EditDilemmaPresenter(@NonNull EditDilemmaFragment view, @NonNull Context context) {
        mView = view;
        mAsyncQueryHandler = new EditDilemmaAsyncQueryHandler(this, context.getContentResolver());
    }

    @Override
    public void addNewDilemma(String dilemmaDescription) {
        if (dilemmaDescription.length() == 0) {
            return;
        }

        ContentValues contentValues = new ContentValues();

        // Заполняем только поле с описанием дилеммы и с timestamp,
        // для остальных полей есть значения DEFAULT,
        // для timestamp почему-то DEFAULT пишет только год, поэтому юзаю AppDateUtils
        contentValues.put(DbContract.DilemmaEntry.COLUMN_DESCRIPTION, dilemmaDescription);
        contentValues.put(DbContract.DilemmaEntry.COLUMN_TIMESTAMP, AppDateUtils.getGmtForCurrentTime());

        mAsyncQueryHandler.startInsert(INSERT_DILEMMA_TOKEN, null, DbContract.DilemmaEntry.CONTENT_URI, contentValues);
    }

    @Override
    public void updateDilemma(String dilemmaId, String dilemmaDescription) {
        if (dilemmaDescription.length() == 0) {
            return;
        }

        updatedDilemmaId = dilemmaId;

        ContentValues contentValues = new ContentValues();

        contentValues.put(DbContract.DilemmaEntry.COLUMN_DESCRIPTION, dilemmaDescription);
        contentValues.put(DbContract.DilemmaEntry.COLUMN_TIMESTAMP, AppDateUtils.getGmtForCurrentTime());

        Uri uri = DbContract.DilemmaEntry.CONTENT_URI;
        uri = uri.buildUpon().appendPath(dilemmaId).build();

        mAsyncQueryHandler.startUpdate(UPDATE_DILEMMA_TOKEN, null, uri, contentValues, null, null);
    }

    private void onInsertCompleted(int token, Uri uri) {
        switch (token) {
            case INSERT_DILEMMA_TOKEN: {
                if (uri != null) {
                    String dilemmaId = uri.getLastPathSegment();
                    mView.onDilemmaInserted(dilemmaId);
                }
                break;
            }
        }
    }

    private void onUpdateCompleted(int token, int updatedRows) {
        switch (token) {
            case UPDATE_DILEMMA_TOKEN: {
                mView.onDilemmaUpdated(updatedDilemmaId);
                break;
            }
        }
    }

    private static class EditDilemmaAsyncQueryHandler extends AsyncQueryHandler {

        private final WeakReference<EditDilemmaPresenter> mPresenterRef;

        EditDilemmaAsyncQueryHandler(EditDilemmaPresenter presenter, ContentResolver cr) {
            super(cr);

            // берём weak reference что бы потом по завершении операций вставки,
            // обновления или удаления можно было вызвать методы класса EditDilemmaPresenter,
            // если его экземпляр ещё не был удалён
            mPresenterRef = new WeakReference<>(presenter);
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            super.onInsertComplete(token, cookie, uri);

            EditDilemmaPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onInsertCompleted(token, uri);
            }
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            super.onUpdateComplete(token, cookie, result);

            EditDilemmaPresenter presenter = mPresenterRef.get();
            if (presenter != null) {
                presenter.onUpdateCompleted(token, result);
            }
        }
    }
}
