package com.samuelunknown.proscons.edit_dilemma;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.samuelunknown.proscons.dilemma.DilemmaActivity;
import com.samuelunknown.proscons.R;
import com.samuelunknown.proscons.utilities.PreferenceUtils;
import com.samuelunknown.proscons.utilities.StatusBarUtils;
import com.samuelunknown.proscons.utilities.ToolBarUtils;

public class EditDilemmaActivity
        extends AppCompatActivity
        implements EditDilemmaFragment.OnEditDilemmaFragmentListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = EditDilemmaActivity.class.getSimpleName();

    public static final String INTENT_DILEMMA_ID_KEY = "DilemmaId";
    public static final String INTENT_DILEMMA_DESCRIPTION_KEY = "CurrentDilemmaDescription";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceUtils.setActivityTheme(this, this, true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dilemma);

        Intent intentThatStartedThisActivity = getIntent();
        if (intentThatStartedThisActivity != null) {

            if (intentThatStartedThisActivity.hasExtra(INTENT_DILEMMA_ID_KEY) &&
                intentThatStartedThisActivity.hasExtra(INTENT_DILEMMA_DESCRIPTION_KEY)) {
                // редактируется существующая дилемма
                // в таком случае должны быть данные по обоим ключам
                // INTENT_DILEMMA_ID_KEY и INTENT_DILEMMA_DESCRIPTION_KEY
                String dilemmaIdString =
                        intentThatStartedThisActivity.getStringExtra(INTENT_DILEMMA_ID_KEY);
                String dilemmaDescriptionString =
                        intentThatStartedThisActivity.getStringExtra(INTENT_DILEMMA_DESCRIPTION_KEY);

                addEditDilemmaFragment(dilemmaIdString, dilemmaDescriptionString);
            } else {
                // создаётся новая дилемма
                addEditDilemmaFragment();
            }
        } else {
            throw new NullPointerException("Intent is null in " + this.getClass());
        }

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, R.id.edit_dilemma_activity_toolbar);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, true);
        ToolBarUtils.setDisplayShowHomeEnabled(this, true);
        //endregion

        //region Toolbar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion
    }

    @Override
    public void onDilemmaUpdated(String dilemmaId) {
        finish();
    }

    @Override
    public void onDilemmaInserted(String dilemmaId) {
        startDilemmaActivity(this, Long.valueOf(dilemmaId));
        finish();
    }

    private void addEditDilemmaFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditDilemmaFragment editDilemmaFragment =
                (EditDilemmaFragment) fragmentManager.findFragmentById(R.id.edit_dilemma_fragment_container);

        if (editDilemmaFragment == null) {
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.edit_dilemma_fragment_container, new EditDilemmaFragment())
                    .commit();
        } else {
            // P.S. при повороте телефона в режим landscape и обратно
            // fragment отсоединяется от контейнера, но так же автоматом и аттачится туда,
            // и поэтому по идее код этого условия не выполняется.. но я не уверен везде ли он
            // автоматом аттачится, так что пусть пока будет.
            if (editDilemmaFragment.isDetached()) {
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .attach(editDilemmaFragment)
                        .commit();
            }
        }
    }

    private void addEditDilemmaFragment(String dilemmaId, String dilemmaDescription) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditDilemmaFragment editDilemmaFragment =
                (EditDilemmaFragment) fragmentManager.findFragmentById(R.id.edit_dilemma_fragment_container);

        if (editDilemmaFragment == null) {
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.edit_dilemma_fragment_container, EditDilemmaFragment.newInstance(dilemmaId, dilemmaDescription))
                    .commit();
        } else {
            // P.S. при повороте телефона в режим landscape и обратно
            // fragment отсоединяется от контейнера, но так же автоматом и аттачится туда,
            // и поэтому по идее код этого условия не выполняется.. но я не уверен везде ли он
            // автоматом аттачится, так что пусть пока будет.
            if (editDilemmaFragment.isDetached()) {
                fragmentManager.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .attach(editDilemmaFragment)
                        .commit();
            }
        }
    }

    private void startDilemmaActivity(@NonNull Context context, long dilemmaId) {
        Intent intent = new Intent(context, DilemmaActivity.class);
        intent.putExtra(DilemmaActivity.INTENT_DILEMMA_ID_KEY, String.valueOf(dilemmaId));
        startActivity(intent);
    }
}
