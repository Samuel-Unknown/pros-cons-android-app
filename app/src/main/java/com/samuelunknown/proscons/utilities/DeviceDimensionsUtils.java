package com.samuelunknown.proscons.utilities;


import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Класс для работы с разрешениями, см. методы кароч
 */
public class DeviceDimensionsUtils {
    /**
     * Возвращает ширину экрана в px
     * @param context контекст
     * @return ширина экрана в px
     */
    public static int getDisplayWidth(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    /**
     * Возвращает высоту экрана в px
     * @param context контекст
     * @return высота экрана в px
     */
    public static int getDisplayHeight(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    /**
     * Переводит dp в px
     * @param dp значение в dp для перевода в px
     * @param context контекст
     * @return px соответствующие dp заданным в аргументе
     */
    public static float convertDpToPixel(float dp, @NonNull Context context){
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    /**
     * Переводит px в dp
     * @param px значение в px для перевода в dp
     * @param context контекст
     * @return dp соответствующие px заданным в аргументе
     */
    public static float convertPixelsToDp(float px, @NonNull Context context){
        Resources r = context.getResources();
        DisplayMetrics metrics = r.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }
}