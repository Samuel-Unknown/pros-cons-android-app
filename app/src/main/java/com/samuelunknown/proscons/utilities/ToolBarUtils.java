package com.samuelunknown.proscons.utilities;

import android.support.annotation.IdRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class ToolBarUtils {

    /**
     * Устанавливает Toolbar в качестве ActionBar
     * @param activity активити
     * @param toolbarId id из ресурсов
     * @return установленный Toolbar
     */
    public static Toolbar setToolbarAsActionBar(AppCompatActivity activity, @IdRes int toolbarId) {
        Toolbar toolbar = (Toolbar) activity.findViewById(toolbarId);
        activity.setSupportActionBar(toolbar);

        return toolbar;
    }

    public static void setDisplayHomeAsUpEnabled(AppCompatActivity activity,
                                                 boolean displayHomeAsUpEnabled) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled);
        }
    }

    public static void setDisplayShowHomeEnabled(AppCompatActivity activity,
                                                 boolean displayShowHomeEnabled) {
        ActionBar actionBar = activity.getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(displayShowHomeEnabled);
        }
    }
}
