package com.samuelunknown.proscons.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.samuelunknown.proscons.R;

/**
 * Класс для работы с настройками приложения.
 * Пока что в настройках можно только выбрать UI тему для приложения (Светлую или Тёмную)
 */
public final class PreferenceUtils {

    /**
     * Утсанавливает тему для активити (Светлую или Тёмную)
     * @param activity активити для которой будет установлена тема
     * @param theme тема которую следует установить для активити
     * @return установленная тема
     */
    private static int setActivityTheme(Activity activity, int theme) {
        activity.setTheme(theme);

        return theme;
    }

    /**
     * Устанавливает тему для активити такой, какая выбранна в приложении (Светлая или Тёмная)
     * @param activity активити для которой будет установлена тема
     * @param context контекст
     * @param noActionBar true - если нужна тема без ActionBar'а, false - если нужна тема с ActionBar'ом
     * @return установленная тема
     */
    public static int setActivityTheme(Activity activity, Context context, boolean noActionBar) {
        int theme = getCurrentAppThemeFormPreference(context, noActionBar);

        return setActivityTheme(activity, theme);
    }

    /**
     * Возвращает установленную в приложении тему (Светлую или Тёмную)
     * @param context контекст
     * @param noActionBar true - если нужна тема без ActionBar'а, false - если нужна тема с ActionBar'ом
     * @return установленная тема
     */
    private static int getCurrentAppThemeFormPreference(Context context, boolean noActionBar) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        String themeName = sharedPreferences.getString(
                context.getString(R.string.pref_app_theme_key),
                context.getString(R.string.pref_app_theme_light_value));

        int theme;

        if (noActionBar) {
            if (context.getString(R.string.pref_app_theme_light_value).equals(themeName)) {
                theme = R.style.AppTheme_Light_NoActionBar;
            } else if (context.getString(R.string.pref_app_theme_dark_value).equals(themeName)) {
                theme = R.style.AppTheme_Dark_NoActionBar;
            } else {
                theme = R.style.AppTheme_Light_NoActionBar;
            }
        } else {
            if (context.getString(R.string.pref_app_theme_light_value).equals(themeName)) {
                theme = R.style.AppTheme_Light_ActionBar;
            } else if (context.getString(R.string.pref_app_theme_dark_value).equals(themeName)) {
                theme = R.style.AppTheme_Dark_ActionBar;
            } else {
                theme = R.style.AppTheme_Light_ActionBar;
            }
        }

        return theme;
    }
}
