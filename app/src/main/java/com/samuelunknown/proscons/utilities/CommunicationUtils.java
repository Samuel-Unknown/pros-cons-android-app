package com.samuelunknown.proscons.utilities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.samuelunknown.proscons.R;

public class CommunicationUtils {

    public static void openRateUsActivity(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        // goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
        //         Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
        //         Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" +
                            context.getPackageName())));
        }
    }

    public static void openWriteToDeveloperActivity(Context context) {
        String email = context.getString(R.string.developer_email);
        String subject = context.getString(R.string.email_subject);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.fromParts("mailto", email, null));
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, R.string.email_error, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Файл настроек отвечающий за проверку отзывов о приложении
     */
    private final static String LAUNCHES_PREFERENCES_FILE = "LAUNCHES_PREFERENCES_FILE";
    /**
     * Ключ для получения числа запусков с последнего раза когда показывали просьбу оставить отзыв
     */
    private final static String LAUNCHES_COUNT_KEY = "LAUNCHES_COUNT_KEY";
    /**
     * Ключ для получения информации о том был ли отсавлен отзыв о приложении
     */
    private final static String APP_RATED_KEY = "APP_RATED_KEY";

    // Количество запусков перед тем, как попросить человека оценить приложение,
    // если оценить человек отказался, то через три запуска его снова об этом попросят
    private final static int LAUNCHES_UNTIL_RATE_US_DIALOG_SHOWING = 3;

    public static void checkForRateUs(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(LAUNCHES_PREFERENCES_FILE, 0);
        SharedPreferences.Editor editor = prefs.edit();

        if (getAppRated(prefs)) {
            return;
        }

        int launchesCount = incrementLaunchesCount(prefs, editor);

        if (launchesCount < LAUNCHES_UNTIL_RATE_US_DIALOG_SHOWING) {
            return;
        }

        resetLaunchesCount(editor);

        showRateUsDialog(context, editor);
    }

    /**
     * Оставлен ли отзыв о приложении
     * @param prefs SharedPreferences
     * @return true - если отсавлен, false - если нет
     */
    private static boolean getAppRated(final SharedPreferences prefs) {
        return prefs.getBoolean(APP_RATED_KEY, false);
    }

    /**
     * Записывает в настройки информацию о том что отзыв оставлен
     * @param editor SharedPreferences.Editor
     */
    private static void setAppRated(final SharedPreferences.Editor editor) {
        editor.putBoolean(APP_RATED_KEY, true);
        editor.apply();
    }

    private static void resetLaunchesCount(final SharedPreferences.Editor editor) {
        editor.putInt(LAUNCHES_COUNT_KEY, 0);
        editor.apply();
    }

    private static int incrementLaunchesCount(final SharedPreferences prefs, final SharedPreferences.Editor editor) {
        int launchesCount = prefs.getInt(LAUNCHES_COUNT_KEY, 0) + 1;

        editor.putInt(LAUNCHES_COUNT_KEY, launchesCount);
        editor.apply();

        return launchesCount;
    }

    private static void showDialog(Context context, String question,
                                   String answerYes, String answerNo,
                                   @Nullable DialogInterface.OnClickListener positiveButtonListener,
                                   @Nullable DialogInterface.OnClickListener negativeButtonListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(question);

        builder.setPositiveButton(answerYes, positiveButtonListener);
        builder.setNegativeButton(answerNo, negativeButtonListener);
        builder.setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private static void showRateUsDialog(final Context context, final SharedPreferences.Editor editor) {

        // Логика работы:
        // Спрашиваю "Нравится ли приложение?"
        //      если да  -> спрашиваю "Оставите ли отзыв?"
        //          если да  -> открываю окно плеймаркета
        //          если нет -> закрываю диалоговое окно
        //      если нет -> спрашиваю "Может есть у вас пожелания?"
        //          если да  -> открываю окно написания email разработчику
        //          если нет -> закрываю диалоговое окно


        final String enjoyQuestion = context.getString(R.string.enjoy_app_question);
        final String enjoyQuestionAnswerYes = context.getString(R.string.enjoy_app_question_answer_yes);
        final String enjoyQuestionAnswerNo = context.getString(R.string.enjoy_app_question_answer_no);

        final String rateUsQuestion = context.getString(R.string.rate_us_question);
        final String rateUsQuestionAnswerYes = context.getString(R.string.rate_us_question_answer_yes);
        final String rateUsQuestionAnswerNo = context.getString(R.string.rate_us_question_answer_no);

        final String feedbackQuestion = context.getString(R.string.feedback_question);
        final String feedbackQuestionAnswerYes = context.getString(R.string.feedback_question_answer_yes);
        final String feedbackQuestionAnswerNo = context.getString(R.string.feedback_question_answer_no);


        final DialogInterface.OnClickListener rateUsQuestionAnswerYesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setAppRated(editor);
                openRateUsActivity(context);
            }
        };

        final DialogInterface.OnClickListener rateUsQuestionAnswerNoListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        };

        final DialogInterface.OnClickListener feedbackQuestionAnswerYesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openWriteToDeveloperActivity(context);
            }
        };

        final DialogInterface.OnClickListener feedbackQuestionAnswerNoListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        };

        final DialogInterface.OnClickListener enjoyQuestionAnswerYesListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDialog(
                        context,
                        rateUsQuestion, rateUsQuestionAnswerYes, rateUsQuestionAnswerNo,
                        rateUsQuestionAnswerYesListener, rateUsQuestionAnswerNoListener);
            }
        };

        final DialogInterface.OnClickListener enjoyQuestionAnswerNoListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDialog(
                        context,
                        feedbackQuestion, feedbackQuestionAnswerYes, feedbackQuestionAnswerNo,
                        feedbackQuestionAnswerYesListener, feedbackQuestionAnswerNoListener);
            }
        };

        showDialog(
                context,
                enjoyQuestion, enjoyQuestionAnswerYes, enjoyQuestionAnswerNo,
                enjoyQuestionAnswerYesListener,
                enjoyQuestionAnswerNoListener);
    }
}
