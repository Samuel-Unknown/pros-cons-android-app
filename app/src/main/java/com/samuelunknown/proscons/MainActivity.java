package com.samuelunknown.proscons;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.samuelunknown.proscons.dilemma.DilemmaActivity;
import com.samuelunknown.proscons.dilemmas.DilemmasContract;
import com.samuelunknown.proscons.dilemmas.DilemmasFragment;
import com.samuelunknown.proscons.edit_dilemma.EditDilemmaActivity;
import com.samuelunknown.proscons.settings.SettingsActivity;
import com.samuelunknown.proscons.utilities.CommunicationUtils;
import com.samuelunknown.proscons.utilities.PreferenceUtils;
import com.samuelunknown.proscons.utilities.StatusBarUtils;
import com.samuelunknown.proscons.utilities.ToolBarUtils;

//TODO Баги:
// region список
// * (UI) в Android 4.1.2 в настройках текст почему-то не имеет отступов
// * в Android 4.1.2 если вкл. приложение, повернуть телефон на бок и обратно, затем поменять
// цветовую схему, то переключение в нав.меню ломается
// endregion

// TODO Сделать:
//region список
//-----------------------------------------------------------------------------------------------------------
// * (UI) анимация активити смены темы должна быть другая
//-----------------------------------------------------------------------------------------------------------
//endregion

// TODO Возможно стоит добавить:
// region список
// * для меню настроек закрывние активити слайдом вправо
// * Анимация ResultView при смене видимсти на Visible ?
// * Перетаскивание аргументов из "За" в "Против" и наоборот ?
// * Перемещение дилемм в архив ?
// * Поддержка планшетов ?
// * Возможность некоторые дилеммы добавить в раздел избранных ?
// * Резервное копирование БД на гугл диск ?
// endregion

// TODO Подумать:
//region список
// * сейчас при переходе из раздела дилемм в раздел корзины, я подменяю фрагменты,
//   а можно же просто перезагружать список дилемм в RecyclerView соответсвующие корзине.. правда
//   подменой можно замутить анимацию собственную, но вряд ли я это стану делать..хм
//endregion

public class MainActivity
        extends
            AppCompatActivity
        implements
            SharedPreferences.OnSharedPreferenceChangeListener,
            NavigationView.OnNavigationItemSelectedListener,
            DilemmasContract.FragmentListener,
            SearchView.OnQueryTextListener {

    //region Variables
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Runnable mPendingRunnableForNavView;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private FloatingActionButton mFab;
    private Toolbar mToolbar;

    private String mCurrentNavMenuSection;

    private DilemmasContract.FragmentView mDilemmasFragmentView;

    @SuppressWarnings({"UnusedDeclaration"})
    private FirebaseAnalytics mFirebaseAnalytics;
    //endregion

    //region Const Values
    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String NAV_MENU_SECTION_DILEMMAS = "Dilemmas";
    private static final String NAV_MENU_SECTION_TRASH = "Trash";
    private static final String CURRENT_NAV_MENU_SECTION_KEY = "CurrentNavMenuSectionKey";

    private static boolean sCheckedForRateUs = false;
    //endregion

    //region Activity Main Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        PreferenceUtils.setActivityTheme(this, this, true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region Check savedInstanceState
        if (savedInstanceState != null) {
            mCurrentNavMenuSection = savedInstanceState.getString(CURRENT_NAV_MENU_SECTION_KEY);
        } else {
            mCurrentNavMenuSection = NAV_MENU_SECTION_DILEMMAS;
        }
        //endregion

        //region FirebaseAnalytics
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //endregion

        //region FAB
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEditDilemmaActivity();
            }
        });
        //endregion
        
        //region NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.nav_dilemmas);
        //endregion

        //region Toolbar
        mToolbar = ToolBarUtils.setToolbarAsActionBar(this, R.id.main_activity_toolbar);
        //endregion

        //region StatusBar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        //region Антибаг исправление бага с названием в тулбаре,
        // установка напрямую не работает в onCreate()
        // решение взято тут -> https://stackoverflow.com/questions/26486730/in-android-app-toolbar-settitle-method-has-no-effect-application-name-is-shown
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (NAV_MENU_SECTION_DILEMMAS.equals(mCurrentNavMenuSection)) {
                actionBar.setTitle(getResources().getText(R.string.title_activity_main_dilemmas_section));
            } else if (NAV_MENU_SECTION_TRASH.equals(mCurrentNavMenuSection)) {
                actionBar.setTitle(getResources().getText(R.string.title_activity_main_trash_section));
            }
        }
        //endregion

        //region DrawerLayout & ActionBarDrawerToggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        final Handler mHandler = new Handler();

        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if (mPendingRunnableForNavView != null) {
                    mHandler.post(mPendingRunnableForNavView);
                    mPendingRunnableForNavView = null;
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        //endregion

        //region Open Current Fragment
        if (NAV_MENU_SECTION_DILEMMAS.equals(mCurrentNavMenuSection)) {
            mNavigationView.setCheckedItem(R.id.nav_dilemmas);
            showAvailableDilemmasFragment();
        } else if (NAV_MENU_SECTION_TRASH.equals(mCurrentNavMenuSection)) {
            mNavigationView.setCheckedItem(R.id.nav_trash);
            showDeletedDilemmasFragment();
        }
        //endregion

        //region OnBackStackChangedListener
        // Что бы при добавлении фрагмента в стек, менялась иконка "гамбургер" на иконку "назад"
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        actionBar.setDisplayHomeAsUpEnabled(true); // show back button
                        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                            }
                        });
                    } else {
                        //show hamburger
                        actionBar.setDisplayHomeAsUpEnabled(false);
                        mActionBarDrawerToggle.syncState();
                        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDrawerLayout.openDrawer(GravityCompat.START);
                            }
                        });
                        showFab();
                    }
                }
            }
        });
        //endregion

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        if (!sCheckedForRateUs) {
            sCheckedForRateUs = true;
            CommunicationUtils.checkForRateUs(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(CURRENT_NAV_MENU_SECTION_KEY, mCurrentNavMenuSection);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    //endregion

    //region Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);

        if (searchItem != null) {
            SearchView mSearchView = (SearchView) searchItem.getActionView();
            mSearchView.setOnQueryTextListener(this);
        }

        switch (mCurrentNavMenuSection) {
            case NAV_MENU_SECTION_TRASH:
                menu.setGroupVisible(R.id.group_trash_section, true);
                break;
            default:
                menu.setGroupVisible(R.id.group_trash_section, false);
                break;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();

        switch (itemThatWasClickedId) {
            case R.id.action_empty_trash:
                emptyTrash();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //endregion

    //region Show Fragments
    public void showAvailableDilemmasFragment() {
        replaceDilemmasFragment(NAV_MENU_SECTION_DILEMMAS);
        showAvailableDilemmasToolbar();
        showFab();
    }

    public void showDeletedDilemmasFragment() {
        replaceDilemmasFragment(NAV_MENU_SECTION_TRASH);
        showDeletedDilemmasToolbar();
        hideFab();
    }

    private void replaceDilemmasFragment(@NonNull String menuSection) throws IllegalArgumentException {

        //region Argument Check
        if (!NAV_MENU_SECTION_DILEMMAS.equals(menuSection) &&
                !NAV_MENU_SECTION_TRASH.equals(menuSection)) {
            throw new IllegalArgumentException("Unknown menuSection: " + menuSection);
        }
        //endregion

        FragmentManager fragmentManager = getSupportFragmentManager();
        DilemmasFragment dilemmasFragment =
                (DilemmasFragment) fragmentManager.findFragmentById(R.id.fragments_container);

        if (dilemmasFragment == null || !menuSection.equals(mCurrentNavMenuSection)) {
            dilemmasFragment =
                    DilemmasFragment.newInstance(NAV_MENU_SECTION_TRASH.equals(menuSection));

            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragments_container, dilemmasFragment)
                    .commit();
        }

        mCurrentNavMenuSection = menuSection;
        mDilemmasFragmentView = dilemmasFragment;
    }

    private void showAvailableDilemmasToolbar() {
        mToolbar.setTitle(getResources().getText(R.string.title_activity_main_dilemmas_section));
        mToolbar.getMenu().setGroupVisible(R.id.group_common, true);
        mToolbar.getMenu().setGroupVisible(R.id.group_trash_section, false);
    }

    private void showDeletedDilemmasToolbar() {
        mToolbar.setTitle(getResources().getText(R.string.title_activity_main_trash_section));
        mToolbar.getMenu().setGroupVisible(R.id.group_common, true);
        mToolbar.getMenu().setGroupVisible(R.id.group_trash_section, true);
    }

    private void removeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragments_container);

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
    }
    //endregion

    //region Hide/Show FAB
    private void hideFab() {
        mFab.hide();
    }

    private void showFab() {
        mFab.show();
    }
    //endregion

    //region Implementation: SearchView.OnQueryTextListener
    @Override
    public boolean onQueryTextChange(String query) {
        if (mDilemmasFragmentView != null) {
            mDilemmasFragmentView.filterByQuery(query);
            return true;
        }

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    //endregion

    //region Implementation: DilemmasContract.FragmentListener
    @Override
    public void onDilemmaSelected(long dilemmaId) {
        startDilemmaActivity(dilemmaId);
    }
    //endregion

    //region Implementation: NavigationView.OnNavigationItemSelectedListener
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dilemmas) {
            //region showAvailableDilemmasFragment
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    showAvailableDilemmasFragment();
                }
            };
            //endregion
        } else if (id == R.id.nav_trash) {
            //region showDeletedDilemmasFragment
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    showDeletedDilemmasFragment();
                }
            };
            //endregion
        } else if (id == R.id.nav_settings) {
            //region startSettingsActivity
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    startSettingsActivity();
                }
            };
            //endregion
        } else if (id == R.id.nav_rate_app) {
            //region openRateUsActivity
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    openRateUs();
                }
            };
            //endregion
        } else if (id == R.id.nav_write_to_developer) {
            //region openWriteToDeveloperActivity
            mPendingRunnableForNavView = new Runnable() {
                @Override
                public void run() {
                    openWriteToDeveloper();
                }
            };
            //endregion
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    //endregion

    //region Implementation: SharedPreferences.OnSharedPreferenceChangeListener
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_app_theme_key))) {
            recreate();
        }
    }
    //endregion

    private void startDilemmaActivity(long dilemmaId) {
        Intent intent = new Intent(this, DilemmaActivity.class);
        intent.putExtra(DilemmaActivity.INTENT_DILEMMA_ID_KEY, String.valueOf(dilemmaId));

        if (mCurrentNavMenuSection.equals(NAV_MENU_SECTION_TRASH)) {
            intent.putExtra(DilemmaActivity.INTENT_DELETED_KEY, true);
        } else {
            intent.putExtra(DilemmaActivity.INTENT_DELETED_KEY, false);
        }

        startActivity(intent);
    }

    private void startEditDilemmaActivity() {
        Intent intent = new Intent(this, EditDilemmaActivity.class);
        startActivity(intent);
    }

    public void startSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void openRateUs() {
        CommunicationUtils.openRateUsActivity(this);
    }

    public void openWriteToDeveloper() {
        CommunicationUtils.openWriteToDeveloperActivity(this);
    }

    private void emptyTrash() {
        mDilemmasFragmentView.emptyTrash();
    }
}
