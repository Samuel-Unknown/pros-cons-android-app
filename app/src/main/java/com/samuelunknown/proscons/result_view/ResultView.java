package com.samuelunknown.proscons.result_view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.samuelunknown.proscons.R;

import java.util.Locale;

/**
 * Класс для отображения результата за/против
 */
public class ResultView extends View {

    //region Variables
    private int mProsShapeColor;
    private int mConsShapeColor;
    private int mProsTextColor;
    private int mConsTextColor;

    private boolean mDisplayPercentages;

    private Paint mPaintProsShape;
    private Paint mPaintConsShape;
    private Paint mPaintProsText;
    private Paint mPaintConsText;

    private int mProsShapeWidth;
    private int mConsShapeWidth;
    private int mProsShapeHeight;
    private int mConsShapeHeight;

    private Rect mProsRect = new Rect();
    private Rect mConsRect = new Rect();
    private Rect mProsTextRect = new Rect();
    private Rect mConsTextRect = new Rect();

    private int mTextSizeInPx;

    // Значения от 0 до 100
    private float prosPercent = 50;
    private float consPercent = 50;

    private static final String TAG = ResultView.class.getSimpleName();
    //endregion

    public boolean isDisplayingPercentages() {
        return mDisplayPercentages;
    }

    public void setDisplayingPercentages(boolean state) {
        this.mDisplayPercentages = state;
        invalidate();
        requestLayout();
    }

    public int getProsShapeColor() {
        return mProsShapeColor;
    }

    public void setProsShapeColor(int color) {
        this.mProsShapeColor = color;
        invalidate();
        requestLayout();
    }

    public int getConsShapeColor() {
        return mConsShapeColor;
    }

    public void setConsShapeColor(int color) {
        this.mConsShapeColor = color;
        invalidate();
        requestLayout();
    }

    public int getProsTextColor() {
        return mProsTextColor;
    }

    public void setProsTextColor(int color) {
        this.mProsTextColor = color;
        invalidate();
        requestLayout();
    }

    public int getConsTextColor() {
        return mConsTextColor;
    }

    public void setConsTextColor(int color) {
        this.mConsTextColor = color;
        invalidate();
        requestLayout();
    }

    public int getTextSize() {
        return mTextSizeInPx;
    }

    public void setTextSize(int sizeInPx) {
        this.mTextSizeInPx = sizeInPx;
        invalidate();
        requestLayout();
    }

    public float getProsPercent() {
        return prosPercent;
    }

    private void setProsPercent(float percent) {

        this.prosPercent = percent;
        invalidate();
        requestLayout();
    }

    public float getConsPercent() {
        return consPercent;
    }

    private void setConsPercent(float percent) {

        this.consPercent = percent;
        invalidate();
        requestLayout();
    }

    /**
     * Установка процентов За и Против (в сумме должны давать 100)
     * @param prosPercent проценты За
     * @param consPercent проценты Против
     */
    public void setProsAndConsPercent(float prosPercent, float consPercent) {

        if (prosPercent < 0) {
            Log.w(TAG, "prosPercent < 0 in setProsAndConsPercent()");
        } else if (prosPercent > 100) {
            Log.w(TAG, "prosPercent > 100 in setProsAndConsPercent()");
        }

        if (consPercent < 0) {
            Log.w(TAG, "consPercent < 0 in setProsAndConsPercent()");
        } else if (consPercent > 100) {
            Log.w(TAG, "consPercent > 100 in setProsAndConsPercent()");
        }

        setProsPercent(prosPercent);
        setConsPercent(consPercent);
    }

    // Этот конструктор позволяет UI создавать и редактировать экземпляр нашего View
    public ResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupAttributes(attrs);
        setupPaint();
    }

    private void setupAttributes(AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.ResultView, 0, 0);

        try {
            mProsShapeColor = a.getColor(R.styleable.ResultView_prosShapeColor, Color.BLUE);
            mConsShapeColor = a.getColor(R.styleable.ResultView_consShapeColor, Color.RED);
            mProsTextColor = a.getColor(R.styleable.ResultView_prosTextColor, Color.RED);
            mConsTextColor = a.getColor(R.styleable.ResultView_consTextColor, Color.BLUE);
            mDisplayPercentages = a.getBoolean(R.styleable.ResultView_displayPercentages, false);
            mTextSizeInPx = a.getDimensionPixelSize(R.styleable.ResultView_textSize, 20);
        } finally {
            a.recycle();
        }
    }

    private void setupPaint() {
        mPaintProsShape = new Paint();
        mPaintConsShape = new Paint();
        mPaintProsText = new Paint();
        mPaintConsText = new Paint();

        mPaintProsShape.setStyle(Paint.Style.FILL);
        mPaintConsShape.setStyle(Paint.Style.FILL);
        mPaintProsText.setStyle(Paint.Style.FILL);
        mPaintConsText.setStyle(Paint.Style.FILL);

        mPaintProsShape.setColor(mProsShapeColor);
        mPaintConsShape.setColor(mConsShapeColor);
        mPaintProsText.setColor(mProsTextColor);
        mPaintConsText.setColor(mConsTextColor);

        mPaintProsText.setTextSize(mTextSizeInPx);
        mPaintConsText.setTextSize(mTextSizeInPx);

        mPaintProsText.setTextAlign(Paint.Align.CENTER);
        mPaintConsText.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mProsRect.set(
                getPaddingLeft(),
                getPaddingTop(),
                getPaddingLeft() + mProsShapeWidth,
                getPaddingTop() + mProsShapeHeight
        );

        mConsRect.set(
                getPaddingLeft() + mProsShapeWidth,
                getPaddingTop(),
                getPaddingLeft() + mProsShapeWidth + mConsShapeWidth,
                getPaddingTop() + mConsShapeHeight
        );

        canvas.drawRect(mProsRect, mPaintProsShape);
        canvas.drawRect(mConsRect, mPaintConsShape);

        if (mDisplayPercentages) {
            String prosPercentText = "+ " +
                    String.format(Locale.getDefault(), "%.1f", prosPercent) + "%";
            String consPercentText = "- " +
                    String.format(Locale.getDefault(), "%.1f", consPercent) + "%";

            mPaintProsText.getTextBounds(prosPercentText, 0, prosPercentText.length(), mProsTextRect);
            mPaintProsText.getTextBounds(consPercentText, 0, consPercentText.length(), mConsTextRect);

            if (mProsTextRect.height() < mProsRect.height() &&
                    mProsTextRect.width() < mProsRect.width()) {

                int xPosPros = getPaddingLeft() + (mProsRect.width()) / 2;
                int yPosPros = getPaddingTop() +
                        (mProsRect.height() / 2) -
                        (int)((mPaintProsText.descent() + mPaintProsText.ascent()) / 2);

                canvas.drawText(
                        prosPercentText,
                        xPosPros,
                        yPosPros,
                        mPaintProsText);
            }

            if (mConsTextRect.height() < mConsRect.height() &&
                    mConsTextRect.width() < mConsRect.width()) {

                int xPosCons = getPaddingLeft() + mProsRect.width() + (mConsRect.width()) / 2;
                int yPosCons = getPaddingTop() +
                        (mConsRect.height() / 2) -
                        (int)((mPaintConsText.descent() + mPaintConsText.ascent()) / 2);

                canvas.drawText(
                        consPercentText,
                        xPosCons,
                        yPosCons,
                        mPaintConsText);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int height = MeasureSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();

        mProsShapeWidth = (int) (width * prosPercent / 100f);
        mConsShapeWidth = (int) (width * consPercent / 100f);
        mProsShapeHeight = height;
        mConsShapeHeight = mProsShapeHeight;

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
